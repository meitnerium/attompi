%-- 2013-05-24 02:00 --%

%config;
%tamin=-100
%tamax=180
N1REP='sin_400_N1_24_juil_2014';
N8REP='sin_400_N8_24_juil_2014';
cd(N1REP);
cd('calc');
config;
cd('../..');
%config;
n=1;
figure(n);
n=n+1;
ffield=strcat(N1REP,'/calc/field.dat');
lfield=importdata(ffield);
plot(lfield(:,1),lfield(:,2));
print -f1 -painters -dpng -loose matfield.png
%figure(n);
%n=n+1;
fprintf('Begining of the analysis\n');
idata=true;   % does data are imported?
Temp=tamin:tadt:tamax;
ETEMP=Emin:DE:Emax;
[T,E]=meshgrid(Temp,ETEMP);
if (idata)
  
  %T=importdata('fort.9');
  %T=importdata('tap.dat');
  %fprintf('Import of T done\n');
  %E=importdata('fort.10');
  %E=importdata('energ2d.dat');
  %fprintf('Import of E done\n');
  N1file=strcat(N1REP,'/calc/newkin2d2.dat');
  N1fileg=strcat(N1REP,'/calc/newkin2dg.dat');
  N1fileu=strcat(N1REP,'/calc/newkin2du.dat');
  %N1data=importdata('matout.dat');
%  newdata=importdata('newkin2d.dat');
  N1data=importdata(N1file);
  N1data=N1data';
  N1datag=importdata(N1fileg);
  N1datag=N1datag';
  N1datau=importdata(N1fileu);
  N1datau=N1datau';
  N8file=strcat(N8REP,'/calc/newkin2d2.dat');
  N8fileg=strcat(N8REP,'/calc/newkin2dg.dat');
  N8fileu=strcat(N8REP,'/calc/newkin2du.dat');
  N8data=importdata(N8file);
  N8data=N8data';
  N8datag=importdata(N8fileg);
  N8datag=N8datag';
  N8datau=importdata(N8fileu);
  N8datau=N8datau';
  %newdata3=newdata2';
  fprintf('Import of newdata done\n');
  save 'resultats.mat';
  fprintf('the results have been saved to resultats.mat\n');
end
hold off;
%mesh(T,E,newdata);
%view(0,90);
%axis([0 40 0 1]);
%colorbar;
%xlabel('\tau (fs)');
%ylabel('E (eV)');
%set(gca,'FontSize',16);
%set(gca,'XTick',(-30:10:60));
%h = get(gca, 'xlabel');
%set(h,'FontSize',16);
%h = get(gca, 'ylabel');
%set(h,'FontSize',16);
%set(gcf, 'PaperPositionMode', 'auto');
%set(gcf, 'PaperUnits', 'inches', 'PaperSize', [5 4]);
%%print -f2 -painters -dpng -loose kin2d.png
%print -painters -depsc2 -loose finalPlot.eps
%print -painters -dpng -loose finalPlot
%print -painters -dpdf -loose finalPlot.pdf
%print -zbuffer -dpdf -loose finalPlot.pdf
%saveas( gcf(), 'finalPlot.png', 'png' );
figure('unit','pixel','position',[100 100 1024 512],'menubar','none');
%figure(n);
%n=n+1;
h=subplot(1,2,1);
p = get(h, 'pos');
p(1)=p(1)-0.06;
p(3) = p(3) + 0.1;
set(h, 'pos', p);
mesh(T,E,N1data);
view(0,90);
axis([-200 tamax 0 1.2]);
colorbar('westoutside');
%set(gca, 'CLim', [0, 0.15]);
xlabel('\tau (fs)');
ylabel('E (eV)');
set(gca,'FontSize',16);
set(gca,'XTick',(-300:100:600));
h = get(gca, 'xlabel');
set(h,'FontSize',16);
h = get(gca, 'ylabel');
set(h,'FontSize',16);
set(gcf, 'PaperPositionMode', 'auto');
set(gcf, 'PaperUnits', 'inches', 'PaperSize', [10 4]);


%figure(n);
%n=n+1;
h=subplot(1,2,2);
p = get(h, 'pos');
p(1)=p(1)-0.06;
p(3) = p(3) + 0.1;
set(h, 'pos', p);

mesh(T,E,N8data);
set(gca,'Position',get(gca,'position')+[0 0 0 0]);
view(0,90);
axis([-200 tamax 0 1.2]);
colorbar('eastoutside');
%set(gca, 'CLim', [0, 0.15]);
xlabel('\tau (fs)');
%ylabel('E (eV)');
set(gca,'FontSize',16);
set(gca,'XTick',(-100:100:600));
set(gca,'ytick',[])

h = get(gca, 'xlabel');
set(h,'FontSize',16);
h = get(gca, 'ylabel');
set(h,'FontSize',16);
set(gcf, 'PaperPositionMode', 'auto');
set(gcf, 'PaperUnits', 'inches', 'PaperSize', [10 20]);
%print -f3 -painters -dpng -loose N1N8kin2d.png
%hFig = gcf;
%set(hFig, 'Position', [10 10 800 400]);
lambda=0.000079;
cCmEv=8065.5;
cCmUa=8065.5/27.212;
n=8;
e0=0.2644;
photon=1.5694;
omega=(1/lambda)/cCmEv;




test=sin(n*(ETEMP-e0));
f1g=sin(n*(ETEMP-e0)*pi/(2*omega));
f2g=sin((ETEMP-e0)*pi/(2*omega));
filtreG=f1g./f2g;
figure('Name','this is the g channel');
n=n+1;
plot(ETEMP./2,filtreG.*filtreG);



f1u=sin(n*((ETEMP-e0)*pi/(2*omega)-pi/2));
f2u=sin((ETEMP-e0)*pi/(2*omega)-pi/2);
filtreU=f1u./f2u;
figure('Name','this is the u channel');
n=n+1;
plot(ETEMP./2,filtreU.*filtreU);


%	ee=0.001*e+0.000001;

%        f1g[e]=sin(n*(ee-e0)*pi/(2*omega));
%        f2g[e]=sin((ee-e0)*pi/(2*omega));
%        filtreG[e]=f1g[e]/f2g[e];
%        fprintf(fp1,"%f\t%f\n",ee/2,filtreG[e]*filtreG[e]/200);

%        f1u[e]=sin(n*((ee-e0)*pi/(2*omega)-pi/2));
%        f2u[e]=sin((ee-e0)*pi/(2*omega)-pi/2);
%        filtreU[e]=f1u[e]/f2u[e];
%    	fprintf(fp2,"%f\t%f\n",ee/2,filtreU[e]*filtreU[e]/20000);


















figure('unit','pixel','position',[100 100 1024 512],'menubar','none','Name','this is the g channel');


%figure(n);
%n=n+1;
h=subplot(1,2,1);
p = get(h, 'pos');
p(1)=p(1)-0.06;
p(3) = p(3) + 0.1;
set(h, 'pos', p);
mesh(T,E,N1datag);
view(0,90);
axis([-200 tamax 0 1.2]);
colorbar('westoutside');
%set(gca, 'CLim', [0, 0.15]);
xlabel('\tau (fs)');
ylabel('E (eV)');
set(gca,'FontSize',16);
set(gca,'XTick',(-300:100:600));
h = get(gca, 'xlabel');
set(h,'FontSize',16);
h = get(gca, 'ylabel');
set(h,'FontSize',16);
set(gcf, 'PaperPositionMode', 'auto');
set(gcf, 'PaperUnits', 'inches', 'PaperSize', [10 4]);


%figure(n);
%n=n+1;
h=subplot(1,2,2);
p = get(h, 'pos');
p(1)=p(1)-0.06;
p(3) = p(3) + 0.1;
set(h, 'pos', p);

mesh(T,E,N8datag);
set(gca,'Position',get(gca,'position')+[0 0 0 0]);
view(0,90);
axis([-200 tamax 0 1.2]);
colorbar('eastoutside');
%set(gca, 'CLim', [0, 0.15]);
xlabel('\tau (fs)');
%ylabel('E (eV)');
set(gca,'FontSize',16);
set(gca,'XTick',(-100:100:600));
set(gca,'ytick',[]);

h = get(gca, 'xlabel');
set(h,'FontSize',16);
h = get(gca, 'ylabel');
set(h,'FontSize',16);
set(gcf, 'PaperPositionMode', 'auto');
set(gcf, 'PaperUnits', 'inches', 'PaperSize', [10 20]);

%title(gca,'you can also place a title like this');







figure('unit','pixel','position',[100 100 1024 512],'menubar','none','Name','this is the u channel');
%figure(n);
%n=n+1;
h=subplot(1,2,1);
p = get(h, 'pos');
p(1)=p(1)-0.06;
p(3) = p(3) + 0.1;
set(h, 'pos', p);
mesh(T,E,N1datau);
view(0,90);
axis([-200 tamax 0 1.2]);
colorbar('westoutside');
%set(gca, 'CLim', [0, 0.15]);
xlabel('\tau (fs)');
ylabel('E (eV)');
set(gca,'FontSize',16);
set(gca,'XTick',(-300:100:600));
h = get(gca, 'xlabel');
set(h,'FontSize',16);
h = get(gca, 'ylabel');
set(h,'FontSize',16);
set(gcf, 'PaperPositionMode', 'auto');
set(gcf, 'PaperUnits', 'inches', 'PaperSize', [10 4]);


%figure(n);
%n=n+1;
h=subplot(1,2,2);
p = get(h, 'pos');
p(1)=p(1)-0.06;
p(3) = p(3) + 0.1;
set(h, 'pos', p);

mesh(T,E,N8datau);
set(gca,'Position',get(gca,'position')+[0 0 0 0]);
view(0,90);
axis([-200 tamax 0 1.2]);
colorbar('eastoutside');
%set(gca, 'CLim', [0, 0.15]);
xlabel('\tau (fs)');
%ylabel('E (eV)');
set(gca,'FontSize',16);
set(gca,'XTick',(-100:100:600));
set(gca,'ytick',[])

h = get(gca, 'xlabel');
set(h,'FontSize',16);
h = get(gca, 'ylabel');
set(h,'FontSize',16);
set(gcf, 'PaperPositionMode', 'auto');
set(gcf, 'PaperUnits', 'inches', 'PaperSize', [10 20]);











tmp=importdata('fort.10');
x=tmp(:,1);
psiv(:,1)=tmp(:,2);
norm(1)=trapz(x,abs(psiv(:,1)).^2);
tmp=importdata('v1.dat');
v(:,1)=tmp(:,2);
tmp=importdata('v2.dat');
v(:,2)=tmp(:,2);
tmp=importdata('fort.11');
psiv(:,2)=tmp(:,2);
norm(2)=trapz(x,abs(psiv(:,2)).^2);
tmp=importdata('fort.12');
psiv(:,3)=tmp(:,2);
norm(3)=trapz(x,abs(psiv(:,3)).^2);
tmp=importdata('fort.13');
psiv(:,4)=tmp(:,2);
norm(4)=trapz(x,abs(psiv(:,4)).^2);
tmp=importdata('fort.14');
psiv(:,5)=tmp(:,2);
norm(5)=trapz(x,abs(psiv(:,5)).^2);
tmp=importdata('fort.15');
psiv(:,6)=tmp(:,2);
norm(6)=trapz(x,abs(psiv(:,6)).^2);
tmp=importdata('fort.16');
psiv(:,7)=tmp(:,2);
norm(7)=trapz(x,abs(psiv(:,7)).^2);
tmp=importdata('fort.17');
psiv(:,8)=tmp(:,2);
norm(8)=trapz(x,abs(psiv(:,8)).^2);
tmp=importdata('fort.18');
psiv(:,9)=tmp(:,2);
norm(9)=trapz(x,abs(psiv(:,9)).^2);
tmp=importdata('fort.19');
psiv(:,10)=tmp(:,2);
norm(10)=trapz(x,abs(psiv(:,10)).^2);
tmp=importdata('fort.20');
psiv(:,11)=tmp(:,2);
norm(11)=trapz(x,abs(psiv(:,11)).^2);
tmp=importdata('fort.21');
psiv(:,12)=tmp(:,2);
norm(12)=trapz(x,abs(psiv(:,12)).^2);
tmp=importdata('fort.22');
psiv(:,13)=tmp(:,2);
norm(13)=trapz(x,abs(psiv(:,13)).^2);
tmp=importdata('fort.23');
psiv(:,14)=tmp(:,2);
norm(14)=trapz(x,abs(psiv(:,14)).^2);
tmp=importdata('fort.24');
psiv(:,15)=tmp(:,2);
norm(15)=trapz(x,abs(psiv(:,15)).^2);
tmp=importdata('fort.25');
psiv(:,16)=tmp(:,2);
norm(16)=trapz(x,abs(psiv(:,16)).^2);
tmp=importdata('fort.26');
psiv(:,17)=tmp(:,2);
norm(17)=trapz(x,abs(psiv(:,17)).^2);
tmp=importdata('fort.27');
psiv(:,18)=tmp(:,2);
norm(18)=trapz(x,abs(psiv(:,18)).^2);
tmp=importdata('fort.28');
psiv(:,19)=tmp(:,2);
norm(19)=trapz(x,abs(psiv(:,19)).^2);
tmp=importdata('fort.29');
psiv(:,20)=tmp(:,2);
norm(20)=trapz(x,abs(psiv(:,20)).^2);
magn=1000;
tmp=importdata('spectr.input');
ev=tmp(:,2);
figure(n);
n=n+1;
h1=plot(x,v(:,1),'-',x,v(:,2),'-',x,psiv(:,1)*magn+ev(1),x,psiv(:,2)*magn+ev(2),x,psiv(:,3)*magn+ev(3),x,psiv(:,4)*magn+ev(4),x,psiv(:,5)*magn+ev(5),x,psiv(:,6)*magn+ev(6),x,psiv(:,7)*magn+ev(7),x,psiv(:,8)*magn+ev(8),x,psiv(:,9)*magn+ev(9),x,psiv(:,10)*magn+ev(10),x,psiv(:,11)*magn+ev(11),x,psiv(:,12)*magn+ev(12),x,psiv(:,13)*magn+ev(13));
ylim([-25000 1000])
xlim([0 10])
xlabel('R (a.u.)');
ylabel('E (eV)');
set(gca,'FontSize',16);
set(gca,'XTick',(0:1:10));
h = get(gca, 'xlabel');
set(h,'FontSize',16);
h = get(gca, 'ylabel');
set(h,'FontSize',16);
set(gcf, 'PaperPositionMode', 'auto');
set(gcf, 'PaperUnits', 'inches', 'PaperSize', [5 4]);
print -f3 -painters -dpng -loose psiv_0_13.png
figure(n);
n=n+1;
magn=500;
h2=plot(x,v(:,1),'-',x,v(:,2),'-',x,psiv(:,14)*magn+ev(14),x,psiv(:,15)*magn+ev(15),x,psiv(:,16)*magn+ev(16),x,psiv(:,17)*magn+ev(17),x,psiv(:,18)*magn+ev(18));
ylim([-4000 1000])
xlim([0 20])
xlabel('R (a.u.)');
ylabel('E (eV)');
set(gca,'FontSize',16);
set(gca,'XTick',(0:2:20));
set(gca,'XMinorTick','on')
h = get(gca, 'xlabel');
set(h,'FontSize',16);
h = get(gca, 'ylabel');
set(h,'FontSize',16);
set(gcf, 'PaperPositionMode', 'auto');
set(gcf, 'PaperUnits', 'inches', 'PaperSize', [5 4]);
print -f4 -painters -dpng -loose psiv_14_17.png
figure(n);
n=n+1;
magn=50;
h2=plot(x,v(:,1),'-',x,psiv(:,19)*magn+ev(19),x,psiv(:,20)*magn+ev(20));
%h2=plot(x,v(:,2),'-');

ylim([-50 50])
xlim([0 60])
xlabel('R (a.u.)');
ylabel('E (eV)');
set(gca,'FontSize',16);
set(gca,'XTick',(0:5:60));
h = get(gca, 'xlabel');
set(h,'FontSize',16);
h = get(gca, 'ylabel');
set(h,'FontSize',16);
set(gcf, 'PaperPositionMode', 'auto');
set(gcf, 'PaperUnits', 'inches', 'PaperSize', [5 4]);
print -f5 -painters -dpng -loose psiv_18_19.png
%print -f1 -zbuffer -dpdf -loose finalPlot.pdf
%print -f2 -zbuffer -dpdf -loose psiv_0_13.pdf
%print -f3 -zbuffer -dpdf -loose psiv_14_17.pdf
%print -f2 -painters -dpng -loose psiv_0_13.png
%print -f3 -painters -dpng -loose psiv_14_17.png
%print -f4 -painters -dpng -loose psiv_18_19.png

figure(n);
n=n+1;
tmeanr=importdata('t.dat');
meanr=importdata('meanr.dat');
for n=1,15000
  tameanr(n,:)=T(1,:)
end
mesh(tameanr,tmeanr,meanr);
view(0,90);
axis([-100 150 0 600]);
colorbar;

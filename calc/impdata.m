%-- 2013-05-24 02:00 --%
config;
%tamin=-100
%tamax=180
lfield=importdata('field.dat');
fprintf('Begining of the analysis\n');
idata=true;   % does data are imported?
Temp=tamin:tadt:tamax;
ETEMP=Emin:DE:Emax;
[T,E]=meshgrid(Temp,ETEMP);
if (idata)
  
  %T=importdata('fort.9');
  %T=importdata('tap.dat');
  %fprintf('Import of T done\n');
  %E=importdata('fort.10');
  %E=importdata('energ2d.dat');
  %fprintf('Import of E done\n');
  %newdata=importdata('matout.dat');
%  newdata=importdata('newkin2d.dat');
  newdata2=importdata('newkin2d2.dat');
  newdata3=newdata2';
  newdatag=importdata('newkin2dg.dat');
  newdatag=newdatag';
  newdatau=importdata('newkin2du.dat');
  newdatau=newdatau';
  fprintf('Import of newdata done\n');
  save 'resultats.mat';
  fprintf('the results have been saved to resultats.mat\n');
end
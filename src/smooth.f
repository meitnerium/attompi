        program smooth
        
      include 'limits.h'
      integer nx(nn)
      real*8  cft,sm(ntotal),x(nn,nxmax),xr(nn)

c ------------------------- input block ----------------------------
        open (8,file='smooth.inp')
        read (8,*) cft ! coefficient
        read (8,*) xr(1) ! coefficient
        close (8)

        open (8,file='grid.unf',form='unformatted')
        read (8) ndim,ntot,(nx(j),j=1,ndim)
        do 10 in=1,ndim
          do 10 j=1,nx(in)
 10           read (8) x(in,j)
        close (8)
        do 60 k=1,ntot
         sm(k)=1.D0/( 1.D0+dexp( (x(1,k)-xr(1))/cft ) )
  60   continue

       open (9,file='smooth.unf',form='unformatted')
       write (9) (sm(k),k=1,ntot)
       close (9)

        open (9,file='smooth.dat')
        do 96 j=1,ntot
             write (9,*) x(1,j),sm(j)
 96     continue
        close (9)


       write (*,*) 'smoothing function - ok'

       end

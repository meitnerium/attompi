c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	subroutine results_output(iv,in,ncall,it,nt,nst,p,wfast,atto_norm_coef
     &                           ,dtfs,rrs)
        include 'limits.h'
        real*8 p(*)                    ! impulse grid
        real*8 rrs(*)                   ! rR*sqrt(sin(th))
	real*8 dtfs
        complex*16 wfast(ncm,*)       ! array for storage of analit. propagation results
	real*8 atto_norm_coef
	character*20 fn1,fn2,fn
	
	if (it.eq.nt) then
           fn1='1_imp_d_'//char(iv+48)//'_'//char(in+48)//'.dat'
           fn2='2_imp_d_'//char(iv+48)//'_'//char(in+48)//'.dat'
           fn='imp_d_'//char(iv+48)//'_'//char(in+48)//'.dat'
	   open (19, file=fn1)
           do i=1,nst
	      rrs(i)=abs(wfast(1,i))**2*atto_norm_coef**2
	      write (19,*) p(i),rrs(i)
	   end do
           close(19)
	   open (19, file=fn2)
           do i=1,nst
	      rrs(i)=abs(wfast(2,i))**2*atto_norm_coef**2
	      write (19,*) p(i),rrs(i)
	   end do
           close(19)
	   open (19, file=fn)
           do i=1,nst
	      rrs(i)=(abs(wfast(1,i))**2 + abs(wfast(2,i))**2)*atto_norm_coef**2
	      write (19,*) p(i),rrs(i)
	   end do
           close(19)
           fn='p.dat'
	   open (19, file=fn)
           do i=1,nst
	      write (19,*) p(i)
	   end do
           close(19)
	   

           fn1='1_imp_f_'//char(iv+48)//'_'//char(in+48)//'.dat'
           fn2='2_imp_f_'//char(iv+48)//'_'//char(in+48)//'.dat'
           fn='imp_f_'//char(iv+48)//'_'//char(in+48)//'.dat'
	   open (19, file=fn1)
           do i=1,nst
	      write (19,*) p(i),wfast(1,i)
	   end do
           close(19)
	   open (19, file=fn2)
           do i=1,nst
	      write (19,*) p(i),wfast(2,i)
	   end do
           close(19)
	   open (19, file=fn)
           do i=1,nst
	      write (19,*) p(i),wfast(1,i) + wfast(2,i)
	   end do
           close(19)

           fn1='r_1_imp_f_'//char(iv+48)//'_'//char(in+48)//'.dat'
           fn2='r_2_imp_f_'//char(iv+48)//'_'//char(in+48)//'.dat'
           fn='r_imp_f_'//char(iv+48)//'_'//char(in+48)//'.dat'
	   open (19, file=fn1)
           do i=1,nst
	      write (19,*) p(i),dble(wfast(1,i))
	   end do
           close(19)
	   open (19, file=fn2)
           do i=1,nst
	      write (19,*) p(i),dble(wfast(2,i))
	   end do
           close(19)
	   open (19, file=fn)
           do i=1,nst
	      write (19,*) p(i),dble(wfast(1,i) + wfast(2,i))
	   end do
           close(19)

           fn1='im_1_imp_f_'//char(iv+48)//'_'//char(in+48)//'.dat'
           fn2='im_2_imp_f_'//char(iv+48)//'_'//char(in+48)//'.dat'
           fn='im_imp_f_'//char(iv+48)//'_'//char(in+48)//'.dat'
	   open (19, file=fn1)
           do i=1,nst
	      write (19,*) p(i),dimag(wfast(1,i))
	   end do
           close(19)
	   open (19, file=fn2)
           do i=1,nst
	      write (19,*) p(i),dimag(wfast(2,i))
	   end do
           close(19)
	   open (19, file=fn)
           do i=1,nst
	      write (19,*) p(i),dimag(wfast(1,i) + wfast(2,i))
	   end do
           close(19)

           fn1='abs_1_imp_f_'//char(iv+48)//'_'//char(in+48)//'.dat'
           fn2='abs_2_imp_f_'//char(iv+48)//'_'//char(in+48)//'.dat'
           fn='abs_imp_f_'//char(iv+48)//'_'//char(in+48)//'.dat'
	   open (19, file=fn1)
           do i=1,nst
	      write (19,*) p(i),abs(wfast(1,i))
	   end do
           close(19)
	   open (19, file=fn2)
           do i=1,nst
	      write (19,*) p(i),abs(wfast(2,i))
	   end do
           close(19)
	   open (19, file=fn)
           do i=1,nst
	      write (19,*) p(i),abs(wfast(1,i) + wfast(2,i))
	   end do
           close(19)

	 else
	 
c           do i=1,nst
c	     rrs(i)=(abs(wfast(1,i))**2 + abs(wfast(2,i))**2)*atto_norm_coef**2
c	     rrs(i)=abs(wfast(1,i))**2*atto_norm_coef**2
c	     write (ncall+20,*) rrs(i)
c	   end do
c	   write(15,*) (it-1)*dtfs
c           do i=1,nst
c	      rrs(i)=abs(wfast(2,i))**2*atto_norm_coef**2
c	      write (ncall+60,*) rrs(i)
c	   end do
	 end if
	 
	 
	 end

c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	subroutine results_output_ke (iv,in,ncall,it,nt,nst,p,wfast,atto_norm_coef
     &                           ,dtfs,rrs,m)
        include 'limits.h'
        real*8 p(*)                    ! impulse grid
        real*8 rrs(*)                   ! rR*sqrt(sin(th))
	real*8 dtfs
        complex*16 wfast(ncm,*)       ! array for storage of analit. propagation results
	real*8 atto_norm_coef
	character*20 fn1,fn2,fn
	real*8 m
	
c	open (8,file='units.cft')
c        read (8,*) cm
c        read (8,*) cv,ct
c        close (8)
c
c        open (8,file='at_mass.inp')
c        read (8,*) m
c        close (8)
c	m=m*cm
	
	if (it.eq.nt) then
           fn1='1_imp_d_'//char(iv+48)//'_'//char(in+48)//'.dat'
           fn2='2_imp_d_'//char(iv+48)//'_'//char(in+48)//'.dat'
           fn='imp_d_'//char(iv+48)//'_'//char(in+48)//'.dat'
	   open (19, file=fn1)
           do i=1,nst
	      rrs(i)=abs(wfast(1,i))**2*atto_norm_coef**2
	      write (19,*) p(i)*p(i)/2.d0/m,rrs(i)
	   end do
           close(19)
	   open (19, file=fn2)
           do i=1,nst
	      rrs(i)=abs(wfast(2,i))**2*atto_norm_coef**2
	      write (19,*) p(i)*p(i)/2.d0/m,rrs(i)
	   end do
           close(19)
	   open (19, file=fn)
           do i=1,nst
	      rrs(i)=(abs(wfast(1,i))**2 + abs(wfast(2,i))**2)
     &                *atto_norm_coef**2
	      write (19,*) p(i)*p(i)/2.d0/m,rrs(i)
	   end do
           close(19)
           fn='p.dat'
	   open (19, file=fn)
           do i=1,nst
	      write (19,*) p(i)
	   end do
           close(19)
	   

           fn1='1_imp_f_'//char(iv+48)//'_'//char(in+48)//'.dat'
           fn2='2_imp_f_'//char(iv+48)//'_'//char(in+48)//'.dat'
           fn='imp_f_'//char(iv+48)//'_'//char(in+48)//'.dat'
	   open (19, file=fn1)
           do i=1,nst
c	   	write (19,*) p(i)*p(i)/2.d0/m,p(i)
	      write (19,*) p(i)*p(i)/2.d0/m,wfast(1,i)
	   end do
           close(19)
	   open (19, file=fn2)
           do i=1,nst
c	      write (19,*) p(i)*p(i)/2.d0/m,p(i)
	      write (19,*) p(i)*p(i)/2.d0/m,wfast(2,i)
	   end do
           close(19)
	   open (19, file=fn)
           do i=1,nst
c	      write (19,*) p(i)*p(i)/2.d0/m,p(i)
	      write (19,*) p(i)*p(i)/2.d0/m,wfast(1,i) + wfast(2,i)
	   end do
           close(19)
	 write(*,*) "p=", p(i),"ke= ",p(i)*p(i)/2.d0/m

	 else
	 
c           do i=1,nst
c	     rrs(i)=(abs(wfast(1,i))**2 + abs(wfast(2,i))**2)*atto_norm_coef**2
c	     rrs(i)=abs(wfast(1,i))**2*atto_norm_coef**2
c	     write (ncall+20,*) rrs(i)
c	   end do
c	   write(15,*) (it-1)*dtfs
c           do i=1,nst
c	      rrs(i)=abs(wfast(2,i))**2*atto_norm_coef**2
c	      write (ncall+60,*) rrs(i)
c	   end do
	 end if
	 
	 
	 end

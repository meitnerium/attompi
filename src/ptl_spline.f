        PROGRAM P_VCALC

        parameter (NXMAX=50000)
	parameter (ND=2)


        CHARACTER*20 VFILE
        INTEGER ndim,ntot,nx(nD)
        real*8 ptl(nxmax),mu(nxmax),v(nxmax)
        real*8 rot1,rot2,v2,v1,a,b
        real*8 x(nD,nxmax)
        REAL*8 RL,RR,STEP,de,beta,re
        REAL*8 RmaxBylly,rmb
	real*8 cv,tmp,calc_morse
        logical MORSEPOT
        real*8 xmu12
        MORSEPOT=.FALSE. 
c	MORSEPOT=.TRUE.	
C
      
      RmaxBylly=194.D0
      open (8,file='units.cft')
       read (8,*) tmp,tmp
       read (8,*) cv
      close (8)
      de=22525.695D0*cv
      
      open (8,file='grid.unf',form='unformatted')
        read (8) ndim,ntot,(nx(j),j=1,ndim)
        do in=1,ndim
          do j=1,nx(in)
            read (8) x(in,j)
	    if (x(1,j).lt.RmaxBylly) then
	      nn=j
	    end if
          end do
        end do
      close (8)
      
      n=nx(1)
      rl=x(1,1)
      rr=x(1,nn)
      step=(rr-rl)/(nn-1)
      if (MORSEPOT) then
      open(91,file='v1.dat')
      open(93,file='v1.unf',form='unformatted')
      open(92,file='v2.dat')
      open(94,file='v2.unf',form='unformatted')
      open(96,file='mu.dat')
      open(95,file='mu.unf',form='unformatted')
      do j=1,n
          call pot_spec(v(j), v2, mu(j), x(1,j))
          write (91,*)  x(1,j),v(j)/cv
          write (93)           v(j)
          write (92,*)  x(1,j),v2/cv
          write (94)           v2
        write (96,*)  x(1,j),mu(j)
        write (95)           mu(j)
      end do
      close(91)
      close(93)
      close(92)
      close(94)
      close(95)
      close(96)
      write(*,*) "v1.[dat unf] v2.[dat unf] mu.[dat unf]"
      write(*,*) "created using Ulaval potential"
 
ccccccccccccccccccccccccccccccccc

cccccccccccccccccccccccccccccccccccc
c use vassili potential
cccccccccccccccccccccccccccccccccccc
      else
      VFILE='H2P000.data'
      CALL VCALC(ptl,NN,RL,RR,STEP,VFILE)
      do j=1,nn
            v(j)=ptl(j)+1.D0/x(1,j)+0.5D0
      end do

      if ((x(1,n).gt.RmaxBylly)) then
      v2=v(nn)
      v1=v(nn-1)
      CALL morse_params(de,beta,re,v2,v1,x(1,nn),x(1,nn-1))
      do j=nn,n
          v(j)=calc_morse(de,beta,re,x(1,j))-de
      end do
      end if
 
      open(91,file='v1.dat')
      open(93,file='v1.unf',form='unformatted')
      do j=1,n
          write (91,*)  x(1,j),v(j)/cv
          write (78,*)  x(1,j),v(j)
          write (93)           v(j)
      end do
      close(91)
      close(93)
      write (*,*) 'v1-ok'


      VFILE='H2P100.data'
      CALL VCALC(ptl,NN,RL,RR,STEP,VFILE)

      do j=1,n
          v(j)=ptl(j)+1.D0/x(1,j)+0.5D0
      end do

      if (x(1,n).gt.RmaxBylly) then
      v2=v(nn)
      v1=v(nn-1)
      CALL morse_params(de,beta,re,v2,v1,x(1,nn),x(1,nn-1))
      do j=nn-1,n
          v(j)=calc_morse(de,beta,re,x(1,j))-de
      end do
      end if


      open(92,file='v2.dat')
      open(94,file='v2.unf',form='unformatted')
      do j=1,n
          write (92,*)  x(1,j),v(j)/cv
          write (94)           v(j)
          write (79,*)  x(1,j),v(j)
      end do
      close(92)
      close(94)
      write (*,*) 'v2-ok'

      VFILE='H2dip000_100.data'
      CALL VCALC(mu,N,RL,RR,STEP,VFILE)

      if (x(1,n).gt.RmaxBylly) then
      v2=mu(nn)
      v1=mu(nn-1)
      CALL lin_params(a,b,v2,v1,x(1,nn),x(1,nn-1))
      do j=nn-1,n
          mu(j)=a*x(1,j)+b
      end do
      
      end if
   
      open(94,file='mu.dat')
      open(95,file='mu.unf',form='unformatted')
      do j=1,n
        write (94,*)  x(1,j),-mu(j)
        write (95)           -mu(j)
      end do
      close(95)
      close(94)
      
      write (*,*) 'ptl, mu - ok'
      end if
     
      END                                                                       

C ________________________________________________________________
        SUBROUTINE morse_params(de,beta,re,v2,v1,x2,x1)
	real*8 de,beta,re,v2,v1,x2,x1,f1,f2

	v1=v1+de
	v2=v2+de
	
	f1=dlog(1.D0-dsqrt(v1/de))
	f2=dlog(1.D0-dsqrt(v2/de))
	beta=(f1-f2)/(x2-x1)
	re=(f2+beta*x2)/beta
	END
C ________________________________________________________________
	FUNCTION CALC_MORSE (de,beta,re,r)
	real*8 CALC_MORSE,de,beta,re,r
	
	CALC_MORSE=de*(1-dexp(-beta*(r-re)))**2
	
        RETURN
	END
C ________________________________________________________________
        SUBROUTINE lin_params(a,b,v2,v1,x2,x1)
	real*8 a,b,v2,v1,x2,x1

	a=(v2-v1)/(x2-x1)
	b=v1-a*x1
	
	END
C ________________________________________________________________
        SUBROUTINE ERVCALC(IERR)
        IF (IERR.EQ.1) THEN
                WRITE(*,*)'IERR.EQ.1'
                STOP
        ELSEIF ((IERR.EQ.2).OR.(IERR.EQ.3).OR.(IERR.EQ.4)) THEN
                IF(IERR-3) 2,3,4
 2              WRITE(*,*)'RL .GE. RR'
                STOP
 3              WRITE(*,*)'RL .GT. R(1)'
                STOP
 4              WRITE(*,*)'RR .LT. R(N)'
                STOP
        ELSEIF (IERR.EQ.5) THEN
                WRITE(*,*)'WARNING'
                WRITE(*,*)'NSTEP < 50'
                RETURN
c                STOP
        ENDIF
        END

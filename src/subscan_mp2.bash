#!/bin/bash
#PBS -N ReTest
#PBS -A cjt-923-aa
#PBS -l walltime=02:15:00
#PBS -l nodes=1:ppn=1
#PBS -M meitnerium109@gmail.com
#PBS -m bea
#PBS -j oe
module load mkl64/10.1.3.027 
module load intel64/14.0.0.080
module load openmpi_intel64/1.6.5_intel14
#cd /home/fradion12/git/atto/current/prog/results
cd "${PBS_O_WORKDIR}"
pwd
./really_scan.bash

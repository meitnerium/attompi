c ========================================================================================      
       subroutine analit_prop(dl,tdl,nt,it,dt,tmax,wf,sm,ntot,nc,nx,p,x,
     &                        m,wfast,nst,pi,ncall,ntimp)
        include 'limits.h'
                INCLUDE 'fftw_f77.i'
	integer np              ! grid point numbers in the moment space for analit. propagation
	parameter (np=nxmax)
        integer nst,nx(nn),nr(2)   ! grid point numbers for one axis
        real*8 dx,x(nn,nxmax),x0(nn)            ! Jacobi grid
        real*8 m
        real*8 sm(ntotal),dt2 
        real*8 p(2*nxmax),pi,t,dt,tmax,dp,tf
        real*8 dl(ntmax)                ! Delta(t_i)
        real*8 tdl(ntmax)               ! delta(t_i)
        real*8 d                        ! Delta(t,t_i)
        real*8 d1                       ! integral[Delta(t,t')dt']
        real*8 d2                       ! integral[Delta(t,t')^2 dt']
        real*8 phr
	complex*16 ph
        complex*16 wf(ncm,ntotal)                     ! part in interaction region 
        complex*16 wfa(ncm,2*ntotal)                  ! assymptotic part
        complex*16 fth_in(2*ntotal),fth_out(2*ntotal) !temp for fftw
        complex*16 wfast(ncm,2*ntotal)                ! array for storage of analit. propagation results
        complex*16 fa(ncm,2*ntotal)
	complex*16 zi
        parameter (zi=(0.D0,1.D0))

        integer*8 p_Rnst_in , p_Rnst_out     ! FFTW R    ! 1D - in analit. propagation
        common /plan_Rnst/ p_Rnst_in ,p_Rnst_out    ! for 1D
        real*8 tau,E0
        common /field/ tau,E0

        if (ncall.eq.1) call fftw_f77_create_plan(p_Rnst_in,nst,-1,0)

         ndim=1

	t=(it-0.5)*dt
	tf=(ntimp-1)*dt
	dx=x(1,2)-x(1,1)
        dp=p(2)-p(1)
	
        do i=ntot,nst    ! initialization of the assymptotic part of wf
           wfa(1,i)=(0.D0,0.d0)
           wfa(2,i)=(0.D0,0.D0)
        end do
  
        do i=1,ntot    ! dividing of wafefuction
          do ic=1,nc  ! for each channel
	    wfa(ic,i) =wf(ic,i)*(1.D0-sm(i))      ! assymptotic part
	    wf (ic,i) =wf(ic,i)*sm(i)           ! interaction part 
	  end do
        end do

	if (it.lt.ntimp) then

           ntf2=ntimp/2
           it2=(it+1)/2
	   dt2=dt*2.D0

           call deltas_filon(dl,tdl,t,it2,dt2,tf,ntf2,d,d1,d2)
c           write (98,*) t,d,d1,d2

           do i=1,nst 
	     fa(1,i)=(wfa(1,i)+wfa(2,i))/dsqrt(2.D0)
	     fa(2,i)=(wfa(1,i)-wfa(2,i))/dsqrt(2.D0)
           end do

           do i=1,ntot   ! shift of the functions on (delta(t,t_i))
	     fa(1,i)=fa(1,i)*exp(+zi*d*x(1,i))
	     fa(2,i)=fa(2,i)*exp(-zi*d*x(1,i))
           end do

           do ic=1,nc  ! FFT for each channel
             do jt=1,nst
               fth_in(jt)=fa(ic,jt)
	     end do
             call fftw_f77_one(p_Rnst_in,fth_in,fth_out)
             do jt=1,nst
  	       ph=dsqrt(0.5D0/pi)*dx*exp(-zi*p(jt)*x(1,1)) ! correction after FFT
               fa(ic,jt)=fth_out(jt)*ph
	     end do
           end do

           do i=1,nst 
	     phr=p(i)**2 * (-t) - 2.D0*p(i)*d1 + d2
  	     ph=exp(-0.5D0*phr*zi/m )    ! phase channel 1 in the article
	     fa(1,i)=fa(1,i)*ph

	     phr=p(i)**2 * (-t) + 2.D0*p(i)*d1 + d2
  	     ph=exp(-0.5D0*phr*zi/m )    ! phase channel 2 in the article
	     fa(2,i)=fa(2,i)*ph
           end do
	
           do i=1,nst 
	     wfa(1,i)=(fa(1,i)+fa(2,i))/dsqrt(2.D0)
	     wfa(2,i)=(fa(1,i)-fa(2,i))/dsqrt(2.D0)
           end do

        else  ! (it .gt. ntimp)

           do ic=1,nc  ! for each channel
             do jt=1,nst
               fth_in(jt)=wfa(ic,jt)
	     end do
             call fftw_f77_one(p_Rnst_in,fth_in,fth_out)
             do jt=1,nst
	       ph=dsqrt(0.5D0/pi)*dx*exp(-zi*p(jt)*x(1,1)) ! correction after FFT
               wfa(ic,jt)=fth_out(jt)*ph
	     end do
           end do

           do i=1,nst 
             phr=p(i)**2 * (-t)
	     ph=exp(-0.5D0*phr*zi/m )
	     wfa(1,i)=wfa(1,i)*ph
	     wfa(2,i)=wfa(2,i)*ph
           end do

      end if     
      
      do i=1,nst    
        wfast(1,i)=wfast(1,i)+wfa(1,i)              ! storage
        wfast(2,i)=wfast(2,i)+wfa(2,i)
      end do
  
      call norms(t,nst,dp,wf,wfast,p,x,pi,nc,dx)

      end
c ========================================================================================      
      subroutine deltas_filon(dl,tdl,t,jt,dt,tf,ntimp,
     &                  delta,delta1,delta2)
      include 'limits.h'
      
      integer ntimp                                ! temporal grid dimension
      real*8 tmax,tf,dt                            ! temporal grid parameters
      real*8 field(ntmax),t                         ! field and contour on the time grid
      real*8 tc(ntmax)                          ! time * contour 
      real*8 dl(ntmax)                          ! filon_cos(c)from 0 to time of pulse duration (tmaximp)
      real*8 tdl(ntmax)                          ! filon_cos(c)from 0 to time of pulse duration (tmaximp)
      real*8 d2l(ntmax)                          ! filon_cos(c)from 0 to time of pulse duration (tmaximp)
      real*8 d1c(ntmax)                         ! c*filon_cos(c)from 0 to time of pulse duration (tmaximp)
      real*8 di,df                               ! filon_cos(tc) from 0 to time of pulse duration (tmaximp)
      real*8 d1i,d1f                             ! filon_cos(c)from 0 to time of pulse duration (tmaximp)
      real*8 d2i,d2f                             ! d2(it+1)=d2(it)+d1(it+1)^2*dt
      real*8 d11,d12
      
      real*8 delta                              ! delta from t_i to time of pulse duration (tmaximp)
      real*8 delta1                             ! integral(delta) from t_i to time of pulse duration (tmaximp)
      real*8 delta2                             ! integral(delta^2) from t_i to time of pulse duration (tmaximp)
      real*8 filon_cos,omega
      real*8 simpson      
      
      
      do it=1,ntimp
        d2l(it)=dl(it)**2                           ! \Delta*c (a.u.) (e=1,\hbar=1)   
      end do
      
        di=dl(jt)  ! \Delta (a.u.) (e=1,\hbar=1)   
        d1i=tdl(jt)
        d2i=simpson(d2l,1,jt,dt)

        df=dl(ntimp)  ! \Delta (a.u.) (e=1,\hbar=1)   
        d1f=tdl(ntimp)
        d2f=simpson(d2l,1,ntimp,dt)

        delta=df-di
        delta1 = -t*delta+d1f-d1i
        delta2=df**2 * (tf-t) -2.D0*df*(d1f-d1i)+d2f-d2i


      end
	
c ========================================================================================      

      subroutine deltas_filon_max(dl,tdl,t,jt,dt,tf,ntimp,
     &                  delta,delta1,delta2)
      include 'limits.h'
      
      integer ntimp                                ! temporal grid dimension
      real*8 tmax,tf,dt                            ! temporal grid parameters
      real*8 field(ntmax),t                         ! field and contour on the time grid
      real*8 tc(ntmax)                          ! time * contour 
      real*8 dl(ntmax)                          ! filon_cos(c)from 0 to time of pulse duration (tmaximp)
      real*8 tdl(ntmax)                          ! filon_cos(c)from 0 to time of pulse duration (tmaximp)
      real*8 d2l(ntmax)                          ! filon_cos(c)from 0 to time of pulse duration (tmaximp)
      real*8 d1c(ntmax)                         ! c*filon_cos(c)from 0 to time of pulse duration (tmaximp)
      real*8 d,dm                               ! filon_cos(tc) from 0 to time of pulse duration (tmaximp)
      real*8 d1,d1m                             ! filon_cos(c)from 0 to time of pulse duration (tmaximp)
      real*8 d2,d2m                             ! d2(it+1)=d2(it)+d1(it+1)^2*dt
      real*8 d11,d12
      
      real*8 delta                              ! delta from t_i to time of pulse duration (tmaximp)
      real*8 delta1                             ! integral(delta) from t_i to time of pulse duration (tmaximp)
      real*8 delta2                             ! integral(delta^2) from t_i to time of pulse duration (tmaximp)
      real*8 filon_cos,omega
      real*8 simpson      
      
      
      do it=1,ntimp
        d2l(it)=dl(it)**2                           ! \Delta*c (a.u.) (e=1,\hbar=1)   
      end do
      
        d=dl(jt)  ! \Delta (a.u.) (e=1,\hbar=1)   
        d1=tdl(jt)
        d2=simpson(d2l,1,jt,dt)

        dm=dl(ntimp)  ! \Delta (a.u.) (e=1,\hbar=1)   
        d1m=tdl(ntimp)
        d2m=simpson(d2l,1,ntimp,dt)

        delta  = dm-d
        delta1 = -(-t*delta+d1m-d1)
        delta2 = d2m-d2
        delta2 = dm**2*(tf-t)-2*dm*(dm*(tf-t)-delta1)+d2m-d2

      end
c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        integer function test_wf(it,wf,n,nx,nc,eps,flag)
        include 'limits.h'
	integer flag                     ! remember for next step when it will be even
        integer ndim                     ! problem dimension
        integer n                        ! grid point number for all axis
        integer nx(nn),nc                ! grid point number for one axis
	real*8 eps
        complex*16 wf(ncm,ntotal)        ! propagated on the final potential
	
	
          if (flag.eq.1) then
	   flag=0
           test_wf=1
           return
	  end if
	  
          do 80 ic=1,nc
	    if ((abs(wf(ic,nx(1))))**2.gt.eps) then
  	       if (mod(it,2).eq.1) then
		  write (81, *) it, ic
	          test_wf=ic
		  flag=0
	          return
	       else
	          flag=1
	       end if
	    end if
  80      continue
          test_wf=0
        end

c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      subroutine norms(t,nst,dp,wf,wfast,p,x,pi,nc,dx)

      include 'limits.h'
      real*8 x(nn,nxmax),dx
      real*8 p(2*nxmax),pi,dp,f1,f2,fa1,fa2,t
      real*8 wfa1(2*ntotal),wfa2(2*ntotal)
      real*8 simpson      
      complex*16 wf(ncm,ntotal)
      complex*16 wf0(ncm,ntotal)
      complex*16 fth_in(2*ntotal),fth_out(2*ntotal)
      complex*16 wfast(ncm,2*ntotal)      
      complex*16 ph,zi
      parameter (zi=(0.D0,1.D0))
      integer*8 p_Rnst_in , p_Rnst_out     
      common /plan_Rnst/ p_Rnst_in ,p_Rnst_out    

      nr=nst/4
      do i=1,nr    
        wfa1(i)=abs(wf(1,i))**2
        wfa2(i)=abs(wf(2,i))**2
      end do
      f1=simpson(wfa1,1,nr,dx)
      f2=simpson(wfa2,1,nr,dx)
c      write (11,*) t,f1
c      write (12,*) t,f2
      write (11,*) t,f1+f2
	

c      do ic=1,nc  ! for each channel FFT of internal part
c        do jt=1,nst
c            fth_in(jt)=wf(ic,jt)
c        end do
c        call fftw_f77_one_(p_Rnst_in,fth_in,fth_out)
c        do jt=1,nst
c	  ph=dsqrt(0.5D0/pi)*dx*exp(-zi*p(jt)*x(1,1)) ! correction after FFT
c          wf0(ic,jt)=fth_out(jt)*ph
c        end do
c      end do

      !output for each channels of the norm Psy_I


      !output for each channels of the norm psy_A
      do i=1,nst    
	wfa1(i)=abs(wfast(1,i))**2
	wfa2(i)=abs(wfast(2,i))**2
      end do
      fa1=simpson (wfa1,1,nst,dp)
      fa2=simpson (wfa2,1,nst,dp)
c      write (21,*) t,f1
c      write (22,*) t,f2
      write (12,*) t,fa1+fa2

      !output for each channels of the norm Psy complete
c      do i=1,nst    
c        wfa1(i)=abs(wf0(1,i)+wfast(1,i))**2
c        wfa2(i)=abs(wf0(2,i)+wfast(2,i))**2
c      end do
c      f1=simpson (wfa1,1,nst,dp)
c      f2=simpson (wfa2,1,nst,dp)
c      write (31,*) t,f1
c      write (32,*) t,f2
      write (13,*) t,f1+f2+fa1+fa2

      end
c ========================================================================================      

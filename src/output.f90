      subroutine results_output(iv,in,ncall,it,nt,nst,p,wfast,atto_norm_coef,dtfs,rrs)
      include 'limits.h'
      real*8 p(*)                    ! impulse grid
      !real*8 rrs(*)                   ! rR*sqrt(sin(th))
      real*8 :: rrs(nst)                   ! rR*sqrt(sin(th))
	   real*8 dtfs
      complex*16 wfast(ncm,*)       ! array for storage of analit. propagation results
      real*8 :: atto_norm_coef
      character*20 fn1,fn2,fn
      real*8 m,cev

      open (8,file='units.cft')
      read (8,*) cm
      read (8,*) cv,ct
      close (8)

      open (8,file='at_mass.inp')
      read (8,*) m
      close (8)
      m=m*cm	
      cev=27.212d0
      write(*,*) 'TEST!!!'
      !atto_norm_coef=1.d0

      write(*,*) 'atto_norm_coef',atto_norm_coef

      if (it.eq.nt) then
           fn1='1_imp_d_'//char(iv+48)//'_'//char(in+48)//'.dat'
           fn2='2_imp_d_'//char(iv+48)//'_'//char(in+48)//'.dat'
           fn='imp_d_'//char(iv+48)//'_'//char(in+48)//'.dat'
        open (19, file=fn1)
        write(*,*) 'NSaT',nst

        rrs(1)=0.d0
           do i=1,nst
              write(*,*) 'i',i
             rrs(i)=abs(wfast(1,i))**2*atto_norm_coef**2
           write (19,*) p(i)*p(i)/2.d0/m*cev/2.d0,rrs(i)
        end do
           close(19)
	   open (19, file=fn2)
           do i=1,nst
	      rrs(i)=abs(wfast(2,i))**2*atto_norm_coef**2
	      write (19,*) p(i)*p(i)/2.d0/m*cev/2.d0,rrs(i)
	   end do
           close(19)
	   open (19, file=fn)
           do i=1,nst
	      rrs(i)=(abs(wfast(1,i))**2 + abs(wfast(2,i))**2)*atto_norm_coef**2
	      write (19,*) p(i)*p(i)/2.d0/m*cev/2.d0,rrs(i)
	   end do
           close(19)
           fn='p.dat'
	   open (19, file=fn)
           do i=1,nst
	      write (19,*) p(i)
	   end do
           close(19)
	   

           fn1='1_imp_f_'//char(iv+48)//'_'//char(in+48)//'.dat'
           fn2='2_imp_f_'//char(iv+48)//'_'//char(in+48)//'.dat'
           fn='imp_f_'//char(iv+48)//'_'//char(in+48)//'.dat'
	   open (19, file=fn1)
           do i=1,nst
	      write (19,*) p(i)*p(i)/2.d0/m*cev/2.d0,wfast(1,i)
	   end do
           close(19)
	   open (19, file=fn2)
           do i=1,nst
	      write (19,*) p(i)*p(i)/2.d0/m*cev/2.d0,wfast(2,i)
	   end do
           close(19)
	   open (19, file=fn)
           do i=1,nst
      write (19,*) p(i)*p(i)/2.d0/m*cev/2.d0,wfast(1,i) + wfast(2,i)
	   end do
           close(19)


	 end if
	 
	 
end subroutine results_output

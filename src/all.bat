./ftn_field.bat
./ftn_kin.bat
./ftn_grid.bat
./ftn_inp_prep.bat
./ftn_ptl_spline.bat
./ftn_sm.bat

make scan
ifort -check all -fpe0  -traceback -debug extended -heap-arrays -g -o exp_op exp_op_jc_1d.f
cp exp_op ../calc
cp scan ../calc

cd ../calc
./0_CALC


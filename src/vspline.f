        SUBROUTINE VCALC(U,NU,RL,RR,STEP,VTABDAT)

        parameter (NXMAX=50000)
	
	
        CHARACTER*20 VTABDAT
        INTEGER NU
        REAL*8 U(*),RL,RR,STEP
C
        INTEGER N,IERR,NSTEP
        REAL*8 COEFE
        DOUBLE PRECISION R(nxmax),V(nxmax),B(nxmax),C(nxmax),D(nxmax)
        DOUBLE PRECISION SEVAL
C
        COEFE=1.D0
        IERR=0

        OPEN(UNIT=1,FILE=VTABDAT)
        READ (1,*) N
        IF(N.GT.nxmax) IERR=1
        CALL ERVCALC(IERR)
        READ(1,*) (R(I),V(I),I=1,N)
        CLOSE(UNIT=1)
        IF(RR.LE.RL) IERR=2
        IF(RL.LT.R(1)) IERR=3
        IF(RR.GT.R(N)) IERR=4
        CALL ERVCALC(IERR)
C
  3     NSTEP=INT((RR-RL)/STEP+0.5D0)+1
        IF(NSTEP.LE.50) IERR=5
        IF(NSTEP.GT.NU) IERR=6
        CALL ERVCALC(IERR)
C
c  5     NU=NSTEP
C
c        STEP=COEFL*STEP
c        RL=COEFL*RL
c        DO 6 I=1,N
c        V(I)=V(I)*COEFE
c  6     R(I)=R(I)*COEFL
C
        OPEN(UNIT=1,FILE='res.dat')

        CALL SPLINE (N,R,V,B,C,D)
        RR=RL
        DO 7 I=1,NU
        U(I)=SEVAL(N,RR,R,V,B,C,D)
        WRITE (1,*) RR,U(I)
        RR=RL+DBLE(I)*STEP
  7     CONTINUE

        CLOSE(1)
C
        RETURN
	
        END
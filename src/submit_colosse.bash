#!/bin/bash
#PBS -N ReTest
#PBS -A cjt-923-aa
#PBS -l walltime=05:15:00
#PBS -l nodes=1:ppn=8
#PBS -M meitnerium109@gmail.com
#PBS -m bea
module load libs/mkl/11.1 
#blas-libs/mkl/11.0
module load compilers/intel/14.0
#compilers/intel/2013
module load mpi/openmpi/1.8.3 
#mpi/openmpi/1.6.4_intel
#cd /home/fradion12/git/atto/current/prog/results
cd "${PBS_O_WORKDIR}"
pwd
./really_all.bash

      program field_figger
      
      include 'limits.h'
      
      integer nt,ntimp                          ! temporal grid dimension
      real*8 tmax,dt                            ! temporal grid parameters
      real*8 E0                                 ! amplitude of field electromagnitic 
      real*8 fi                                 ! intensity of field electromagnitic 
      real*8 rfocus
      real*8 lambda,omega                       ! wavelenght & frequecy
      real*8 tau                                ! pulse duration
      real*8 e(ntmax),e1(ntmax),c(ntmax),c1(ntmax),t(ntmax)         ! field and his contour on the time grid
      real*8 cv,ct,cl,cm,pi,cvj,cvi             ! units transormation coefficients
      real*8 field,contour_figger_I
      integer ftype,nsin
      real*8 contour_figger_I_sin2
      
      
        open (8,file='units.cft')
        read (8,*) tmp,tmp
        read (8,*) cv,ct
        close (8)
        cvi=5.33807915d-9              ! W/cm^2 --> a.u.e
      
      open (8,file='dynamics.inp')
       read (8,*) tmax,dt
      close (8)
      nt=dint(tmax/dt)
      
      
    

      open (8,file='field.inp')
       read (8,*) lambda             ! cm
       read (8,*) fi                 ! W*cm^{2}
       read (8,*) tau                ! fs
       read (8,*) tp                ! fs
       read (8,*) ftype                 ! fs (time of the max of pulse)
       read (8,*) nsin                 ! fs (time of the max of pulse)

      close (8)

      omega=1.d0/lambda              ! cm^{-1}
      write (*,*) "omega=",omega
      omega=omega*cv                      ! Eh
      tmax=tmax*ct                   
      dt=dt*ct
      tau=tau*ct
c - test 23/01/2007
c	write(*,*) fi
c	fi=sqrt(fi/(3.52*(10**16)))	      
      write (*,*) fi

! find ntimp
      if (ftype.eq.1) then
      tau=tau/2.D0/dsqrt(dlog(2.D0))
        do it=1,nt
          t(it)=(it-1)*dt
c - test 23/01/2007
        c1(it)=dsqrt(contour_figger_I(t(it),fi,tau,pi))*cvi
	c(it)=(contour_figger_I(t(it),fi,tau,pi))
        if ((c1(it)<1.D-10).and.(it>2)) goto 10
      end do
 10   continue
      ntimp=it
      if (mod(ntimp,2)==1) ntimp=ntimp-1     ! ntimp must be pair for Filon
      ntimp=2*ntimp-1
c! to use the complete time grid
      ntimp=nt 
      
      do it=1,ntimp
        t(it)=(-ntimp/2+it-1)*dt + 0.5D0*dt
c - test 23/01/2007 C.L.
        c(it)=dsqrt(contour_figger_I(t(it),fi,tau,pi))*cvi   ! contour
c        c(it)=dsqrt(contour_figger_I_sin2(t(it),fi,tau,0.d0,
c     & nsin))*cvi 
c	c(it)=(contour_figger_I(t(it),fi,tau,pi))
        t(it)=(it-0.5D0)*dt                                                !!!!!!!!!! +0.5dt for split operator
        e(it)=field(c(it),t(it),omega)            ! field
      end do
      
      ntimp=ntimp-1
      write (*,*) 'number of field poins = ', ntimp
      else if (ftype.eq.2) then
        do it=1,nt
          t(it)=(it-1)*dt
c - test 23/01/2007
        c1(it)=dsqrt(contour_figger_I(t(it),fi,tau,pi))*cvi
	c(it)=(contour_figger_I(t(it),fi,tau,pi))
        if ((c1(it)<1.D-10).and.(it>2)) goto 11
      end do
 11   continue
      ntimp=it
      if (mod(ntimp,2)==1) ntimp=ntimp-1     ! ntimp must be pair for Filon
      ntimp=2*ntimp-1
      ntimp=nt
      
      do it=1,ntimp
        t(it)=(-ntimp/2+it-1)*dt + 0.5D0*dt
c - test 23/01/2007 C.L.
c        c(it)=dsqrt(contour_figger_I(t(it),fi,tau,pi))*cvi   ! contour
        c(it)=dsqrt(contour_figger_I_sin2(t(it),fi,tau,0.d0,
     & nsin))*cvi 
c	c(it)=(contour_figger_I(t(it),fi,tau,pi))
        t(it)=(it-0.5D0)*dt                                                !!!!!!!!!! +0.5dt for split operator
        e(it)=field(c(it),t(it),omega)            ! field
      end do
      
      ntimp=ntimp-1
      end if
      open(9,file='field.dat')
      open(8,file='contour.dat')
      open(91,file='field.unf',form='unformatted')
      open(92,file='field_cont.unf',form='unformatted')
      write (91) ntimp
      write (92) ntimp
      write (92) omega
      do 100 it=1,ntimp
        write (9,*) t(it)/ct,e(it)
        write (8,*) t(it)/ct,c(it)
c!modif CL 17/03/2009
        write (91)  t(it), e(it)
c! fin modif
        write (92) c(it)
 100  continue
      close(8)
      close(9)
      close(91)
      close(92)
      
      write (*,*) 'field - ok'
      
      end
c --------------------------------------------------------------------------------------------------
      real*8 function contour_figger_I(t,E0,tau,pi)
      real*8 t,E0,tau,pi
      contour_figger_I = E0*exp(-(t/tau)**2)
      end
c --------------------------------------------------------------------------------------------------
      real*8 function contour_figger_I0(t,E0,tau,pi)
      real*8 t,E0,tau,pi
      contour_figger_I0 = E0*2.D0*dsqrt(dlog(2.D0))
     &                   * exp(-4.D0*dlog(2.D0)*(t/tau)**2)
      end
c --------------------------------------------------------------------------------------------------
      real*8 function field(c,t,omega)
      real*8 t,omega,c
      field = c*dcos(omega*t)
      end
c --------------------------------------------------------------------------------------------------
       real*8 function contour_figger_I_sin2(t,E0,tau,tp,nsin)
       real*8 t,E0,tau,pi,w,tp,tmp,PER
       integer nsin
       pi=3.141592654d0
       PER=tau*2.d0
       w=2.d0*pi/PER
       if ((t.lt.tp-PER/4.d0).or.(t.gt.tp+PER/4.d0)) then
         contour_figger_I_sin2 = 0.d0
       else
         contour_figger_I_sin2 = dcos(w*(t-tp))**(nsin*nsin)*E0
       end if
c       !      write(68,*) t,tp,PER
c       !      write(69,*)'INSIN2 ',t,contour_figger_I_sin2
       end function 


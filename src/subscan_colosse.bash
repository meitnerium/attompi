#!/bin/bash
#PBS -N ReTest
#PBS -A cjt-923-aa
#PBS -l walltime=00:15:00
#PBS -l nodes=1:ppn=8
#PBS -M meitnerium109@gmail.com
#PBS -m bea
#PBS -j oe
module load libs/mkl/11.1 
module load compilers/intel/14.0
module load mpi/openmpi/1.8.3 
module load apps/gnuplot/4.6.4
#cd /home/fradion12/git/atto/current/prog/results
cd "${PBS_O_WORKDIR}"
pwd
date
#./really_scan.bash
#./all.bat
#make scan
#ifort -check all -fpe0  -traceback -debug extended -heap-arrays -g -o exp_op exp_op_jc_1d.f
#cp scan ../calc
#mv exp_op ../calc
#echo ok
cd ../calc
#./0_CALC
#./scan
mpirun -np 8 ./scan

        subroutine FFTW_init(ndim,nx,nst)
        include 'limits.h'
        INCLUDE 'fftw_f77.i'

        integer nrr,nth,ndim,nr(2),nx(*)
        double complex frr_in(nxmax**ndim),frr_out(nxmax**ndim)

c        integer FFTW_FORWARD,FFTW_BACKWARD
c        integer FFTW_ESTIMATE,FFTW_MEASURE
c        parameter (FFTW_ESTIMATE=0,FFTW_MEASURE=1)
c        parameter (FFTW_FORWARD=-1,FFTW_BACKWARD=1)
        integer*8 p_th_in, p_th_out    ! FFTW theta
        integer*8 p_Rr_in, p_Rr_out    ! FFTW Rr
        integer*8 p_R_in , p_R_out     ! FFTW R    ! 1D
        integer*8 p_Rnst_in , p_Rnst_out     ! FFTW R    ! 1D - in analit. propagation
        
        
        common /plan_th/p_th_in,p_th_out
        common /plan_Rr/p_Rr_in,p_Rr_out
        common /plan_R/ p_R_in ,p_R_out    ! for 1D
        common /plan_Rnst/ p_Rnst_in ,p_Rnst_out    ! for 1D
	
	if(ndim==1) then
          call fftw_f77_create_plan(p_R_in,NX(1),
     &                           FFTW_FORWARD,FFTW_ESTIMATE)
          call fftw_f77_create_plan(p_R_out,NX(1),
     &                           FFTW_BACKWARD,FFTW_ESTIMATE)
          call fftw_f77_create_plan(p_Rnst_in,nst,
     &                           FFTW_FORWARD,FFTW_ESTIMATE)
          call fftw_f77_create_plan(p_Rnst_out,nst,
     &                           FFTW_BACKWARD,FFTW_ESTIMATE)
          return	
	end if

        nr(1)=nx(1)
        nr(2)=nx(2)
        call fftw2d_f77_create_plan(p_Rr_in,nr(1),nr(2),
     &                           FFTW_FORWARD,FFTW_ESTIMATE)
        call fftw2d_f77_create_plan(p_Rr_out,nr(1),nr(2),
     &                           FFTW_BACKWARD,FFTW_ESTIMATE)
        call fftw_f77_create_plan(p_th_in,NX(3),
     &                           FFTW_FORWARD,FFTW_ESTIMATE)
        call fftw_f77_create_plan(p_th_out,NX(3),
     &                           FFTW_BACKWARD,FFTW_ESTIMATE)
	end
c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      subroutine fft (f,fst,nrr,nth,nr,sign,n,nc,var)

      include 'limits.h'
      INCLUDE 'fftw_f77.i'
      integer sign,n,nr(2)
      integer*8 p_in,p_out
      integer*8 p_R_in , p_R_out           ! FFTW R    ! 1D
      integer*8 p_Rnst_in , p_Rnst_out     ! FFTW R    ! 1D - in analit. propagation
      complex*16 f(ncm,ntotal),fst(ncm,2*ntotal),f1(2*ntotal)
      character*2 var
      common /plan_R/ p_R_in ,p_R_out    ! for 1D
      common /plan_Rnst/ p_Rnst_in ,p_Rnst_out    ! for 1D
      do 2 ic=1,nc  ! for each channel
        if (var.eq.'R1')  then
        call chan1(f,f1,n,ic)
	   call fft_R  (p_R_in,p_R_out,f1,sign,n)
        do 10 k=1,n
  10       f(ic,k)=f1(k)
	end if
        if (var.eq.'Rs')  then
           call chan1(fst,f1,n,ic)
	   call fft_R  (p_Rnst_in,p_Rnst_out,f1,sign,n)
        do 20 k=1,n
  20       fst(ic,k)=f1(k)
	end if
  2   continue
      end
c==================================================================
        subroutine FFT_R (p_in,p_out,f,sign,ntot)
        include 'limits.h'
        INCLUDE 'fftw_f77.i'
        integer sign,ntot
        integer*8 p_in,p_out
        complex*16 f(*)
c        double complex fth_in(2*ntotal),fth_out(2*ntotal)
c modif by Franck
        complex*16 fth_in(2*ntotal),fth_out(2*ntotal)
          do 20 jt=1,ntot
            fth_in(jt)=f(jt )
  20      continue

          if (sign.eq.1 ) then
            call fftw_f77_one(p_in,fth_in,fth_out)
            do 30 jt=1,ntot
              f( jt )=fth_out(jt)
  30        continue
          end if
          
          if (sign.eq.-1) then
            call fftw_f77_one(p_out,fth_in,fth_out)
            do 40 jt=1,ntot
              f(jt)=fth_out(jt)/(ntot)
  40        continue
          end if
          
  10    continue
        end
c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        subroutine FFT_th (f,nrr,nth,sign,ntot)
        include 'limits.h'
        INCLUDE 'fftw_f77.i'
        integer nrr,nth,sign,ntot
        integer*8 p_in,p_out
        complex*16 f(*)
        double complex fth_in(nxmax),fth_out(nxmax)
        common /plan_th/p_in,p_out
        do 10 jr=1,nrr
c storage in fth the points depended of theta for one Rr
          do 20 jt=1,nth
            fth_in(jt)=f( (jt-1)*nrr + jr )
  20      continue

          if (sign.eq.1 ) then
            call fftw_f77_one(p_in,fth_in,fth_out)
            do 30 jt=1,nth
              f( (jt-1)*nrr + jr )=fth_out(jt)
  30        continue
          end if
          
          if (sign.eq.-1) then
            call fftw_f77_one(p_out,fth_in,fth_out)
            do 40 jt=1,nth
              f( (jt-1)*nrr + jr )=fth_out(jt)/(nth)
  40        continue
          end if
          
  10    continue
        end
c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        subroutine FFT_Rr (f,nrr,nth,nr,sign,ntot,ndim)
        include 'limits.h'
        INCLUDE 'fftw_f77.i'

        integer nrr,nth,ndim,sign,ntot,nr(2)
        integer*8 p_in,p_out,p_pr_in,p_pr_out
        complex*16 f(*)
        double complex frr_in(nxmax**ndim),frr_out(nxmax**ndim)
        common /plan_Rr/p_in,p_out
        do 10 jt=1,nth
          do 20 jr=1,nrr
            frr_in(jr)=f( (jt-1)*nrr + jr )
  20      continue
          if (sign.eq.1 ) then
            call fftwnd_f77_one(p_in,frr_in,frr_out)
            do 30 jr=1,nrr
              f( (jt-1)*nrr + jr)=frr_out(jr)
  30        continue
          end if
          if (sign.eq.-1) then
            call fftwnd_f77_one(p_out,frr_in,frr_out)
            do 40 jr=1,nrr
              f( (jt-1)*nrr + jr)=frr_out(jr)/(nrr)
  40        continue
          end if
  10    continue
        end
c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

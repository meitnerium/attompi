        program wf_0

        include 'limits.h'
        integer nx(nn)
        real*8 wf(2*ntotal),x0(nn)
        real*8 x(nn,nxmax)
        real*8 tmp
c ----------------- input ---------------
        open (8,file='grid.unf',form='unformatted')
        read (8) ndim,ntot,(nx(j),j=1,ndim)
        do 93 in=1,ndim
          do 93 j=1,nx(in)
             read (8) x(in,j)
 93       continue
        write (*,*) 'input - ok'
c ----------------- calc -----------------
        call wf_calc (ndim,ntot,nx,x,wf)
        write (*,*) 'wf_calc - ok'
        call norming (ndim,ntot,nx,x,wf)
        write (*,*) 'wf_norming - ok'
c ----------------- output ---------------
        open (9,file='wf_0.unf',form='unformatted')
        write (9) (wf(k),k=1,2*ntot)
        close (9)

        call wf_out (x,nx,ndim,ntot,wf)

        write (*,*) 'wf_bound - ok'

        end
c ==========================================================
        subroutine wf_calc (ndim,ntot,nx,x,wf)

        include 'limits.h'

        integer i(nn),nx(nn)
        real*8 alf(nn),wf(*),x(nn,nxmax)
        real*8 sdvig(nn),sum1,sum2,sum3

        open (8,file='3d_gauss.inp')
        read (8,*) (alf(j),sdvig(j),j=1,ndim)
c        write (*,*) (alf(j),sdvig(j),j=1,ndim)
        close(8)

        do 12 j=1,ndim
          i(j)=1         ! counters inition
  12    continue

        do 30 j=1,2*ntot-1,2   ! 1dim array for wf (real part)
          sum1=0.D0
          sum2=0.D0
          sum3=0.D0
          do 15 jj=1,ndim
            sum1=sum1+alf(jj)*(x(jj,i(jj))-sdvig(jj))**2
            sum3=sum3+alf(jj)*x(jj,i(jj))**2
  15        sum2=sum2+alf(jj)*(x(jj,i(jj))+sdvig(jj))**2
        call next_index (i,nx,ndim)   ! next idim point
          wf(j)=exp(-sum1)
c          wf(j)=wf(j)+ exp(-sum2)
c          wf(j)=wf(j)- exp(-sum3)
  30    continue

        do 40 j=2,2*ntot,2   ! 1dim array for wf (im part)
          wf(j)=0.d0
  40    continue

        end

c ==========================================================
        subroutine wf_out (x,nx,ndim,ntot,f)
        include 'limits.h'

        integer nx(*),i(nn)
        real*8 f(*),x(nn,nxmax)
        character*12 fn

          fn='wf_0.dat'
        do 95 j=1,ndim
 95     i(j)=1

        open (9,file=fn)
        do 96 j=1,2*ntot-1,2
             write (9,*) (x(k,i(k)),k=1,ndim),f(j)
        call next_index (i,nx,ndim)
 96     continue
        close (9)

        end
c ==========================================================
        subroutine norming (ndim,ntot,nx,x,wf)
        include 'limits.h'

        integer nx(ndim)
        real*8 wf(*),x(nn,nxmax),podint(2*ntotal)
        real*8 norm

        write (*,*) 'norming started'
        call mult(wf,wf,podint,ntot)
        write (*,*) 'mult _ ok'
        call integral (ndim,x,nx,podint,ntot,norm)

        write (*,*) 'wf_norm=',norm

        do 10 j=1,2*ntot-1,2
  10      wf(j)=wf(j)/sqrt(norm)
c   norm check
        call mult(wf,wf,podint,ntot)
        call integral (ndim,x,nx,podint,ntot,norm)
        write (*,*) 'wf_norm=',abs(norm)
        end
c ==========================================================
        subroutine mult (x,y,z,n)
        real*8 x(*),y(*),z(*),zr,zi

           do 10 k=1,n
             zr=x(2*k-1)*y(2*k-1) - x(2*k)*y(2*k)
             zi=x(2*k-1)*y(2*k) + x(2*k)*y(2*k-1)
             z(2*k-1)=zr
             z(2*k)=zi
  10       continue

        end
c==================================================================
        subroutine next_index (i,nx,ndim)
        include 'limits.h'
        integer i(nn),nx(nn)

        j=1

  10    continue
        if (i(j).ne.nx(j)) then
           i(j)=i(j)+1
           return
        end if

        if (ndim.eq.j) goto 30

        if (i(j+1).ne.nx(j+1)) then
           i(j+1)=i(j+1)+1
           do 20 jj=1,j
  20             i(jj)=1
           return
        end if

        j=j+1
        goto 10

  30    continue
        end
C---------------------------------------------------------------------
       subroutine integral (ndim,x,nx,f,ntot,intg)

       include 'limits.h'

       integer nx(*),ntt,ntot,jj1,jj2,jdim,kk,jj,ndim,k
       real*8 f(*),x(nn,nxmax),dx1
       real*8 x1(nxmax),rf(nxmax),if(nxmax)
       real*8 rintg1,iintg1
       complex*16 intg


       ntt=ntot
       jdim=1
c integrating for the first axes

  15   kk=1
       jj1=1
       jj2=2*nx(jdim)

       dx1=x(jdim,2)-x(jdim,1)

  20   continue
       k=1
       do 30 jj=jj1,jj2,2
                rf(k)=f(jj)
                if(k)=f(jj+1)
                k=k+1
  30   continue

       call int_r1d (dx1,rf,rintg1,nx(jdim))
       call int_r1d (dx1,if,iintg1,nx(jdim))
       f(kk)  =rintg1
       f(kk+1)=iintg1

       jj1=jj1+2*nx(jdim)
       jj2=jj2+2*nx(jdim)

       if (kk+1.ne.2*ntt/nx(jdim)) then
          kk=kk+2
          goto 20
       end if

       ntt=ntt/nx(jdim)
       jdim=jdim+1

       if (jdim-1.ne.ndim) goto 15 ! integrating for the next axes


       intg=cmplx(rintg1,iintg1)

       end

c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      subroutine int_r1d(dx,y,integ,nx)
      integer i,nx
      real*8 dx
      real*8 integ,y(*),sum
c
      sum=0.d0
c
c     integration loop
      do 1 i=2,nx-1
 1     sum=sum+y(i)
      sum=sum+y(1)/2.d0+y(nx)/2.d0
      integ=sum*dx
      end

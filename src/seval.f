	DOUBLE PRECISION FUNCTION SEVAL (N, U, X, Y, B, C, D)
	INTEGER N
	DOUBLE PRECISION  U, X(N), Y(N), B(N), C(N), D(N)
C
C  ��������� ��������� �������� ����������� �������
C
C	SEVAL=Y(I)+B(I)*(U-X(I))+C(I)*(U-X(I))**2+
C	D(I)*(U-X(I))**3
C
C	��� X(I) .LT. U .LT. X(I+1)
C
C	������������ ����� �������
C
C   ���� U .LT. X(1), �� ������� �������� I=1.
C   ���� U .GE. X(1), �� ������� �������� I=N.
C
C   ������� ����������
C
C	N=����� �������� �����
C	U=��������,��� ������� ����������� �������� �������
C	X,Y=������� �������� ������� � �������
C	B,C,D=������� ������������� �������, �����������
C	���������� SPLINE
C
C   ���� �� ��������� � ���������� ������� U �� ���������
C   � ��� �� ���������, �� ��� ��������� ������� ���������
C   ����������� �������� �����.
C
	INTEGER I,J,K
	DOUBLE PRECISION DX
	DATA I/1/
	IF (I .GE. N) I=1
	IF (U .LT. X(I)) GO TO 10
	IF (U .LT. X(I+1)) GO TO 30
C
C	�������� �����
C
  10	I=1
	J=N+1
  20	K=(I+J)/2
	IF (U .LT. X(K)) J=K
	IF (U .GE. X(K)) I=K
	IF (J .GT. I+1) GO TO 20
C
C   ��������� ������
C
  30	DX=U-X(I)
	SEVAL=Y(I)+DX*(B(I)+DX*(C(I)+DX*D(I)))
	RETURN
	END

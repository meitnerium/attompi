        program exp_op
c calculation of exponents of the energy operators
        include 'limits.h'
        integer ndim              ! problem dimension
        integer ntot              ! grid point number for all axis
        real*8 ptl(ncm,ntotal)        ! final state potential surface
        real*8 krr(ntotal)
        real*8 wf(ncm,ntotal)         ! (1/4sin^2(th)+0.25)/2I(R,r)
        complex*16 ekrr(ntotal)
        complex*16 cwf(ncm,ntotal)        ! exp operators of pot. en. + exp (vt)
        real*8 tmax, dt
        character*20 fn

c ====================== input ===================================
        open (8,file='units.cft')
        read (8,*)
        read (8,*) cv,ct
        close (8)

        open (8,file='dynamics.inp')
        read (8,*) tmax,dt
        read (8,*) nc
        close (8)

        write (*,*) 'input txt - ok'

        open (8,file='grid.unf',form='unformatted')
        read (8) ndim,ntot
        close (8)

        write (*,*) 'input grid - ok'

        open (8, file='kin.unf')
        do 20 j=1,ntot
          read (8,*) krr(j)
 20     continue
        close (8)
        write (*,*) 'input oper - ok'

        do 30 j=1,nc

          fn='v'//char(j+48)//'.unf'
          open (8,file=fn,form='unformatted')
          do 25 k=1,ntot
 25         read (8) ptl(j,k)
          close (8)

 30     continue
        
        write (*,*) 'input ptl - ok'
        write (*,*) 'input wf - ok'
        write (*,*) 'input - ok'

c ====================== inition ===============================
        dt=dt*ct
        tmax=tmax*ct
        nt=aint(tmax/dt)

        do 50 j=1,ntot
          ekrr(j)=dcmplx(dcos(krr(j)*dt),-dsin(krr(j)*dt))
  50    continue
c ---------- exp operator of a kinetic energy from I(R,r)*d^2/dth^2 -----------
        write (*,*) 'inition arrays - ok'

c ================ output ====================================

        open (8, file='oper.unf',form='unformatted')
        write (8) 'see exp_oper.unf'
        close (8)
        
        open (8, file='cmplx_oper.unf',form='unformatted')
        do 70 j=1,ntot
          write (8) ekrr(j),(ptl(ic,j),ic=1,nc)
 70     continue
        close (8)
        
        write (*,*) 'output oper - ok'
        
        end
c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


subroutine dyn(ta,iat,ac,na,energ,newkin,king,kinu,dtfs,tmax,ct,rank,ndim,ntot,nx,x,ekrr,ptl,ranorm,meanr)

    !c
    !c   program for solution of the 1D 2 channel Schrodinger equation
    !c   with the time dependent Hamiltonian by a split
    !c   operator metod. This program propagate wafe function of the
    !c   inition state on the both (initial and final) potential surfaces.
    !c
        
        include 'limits.h'
!    integer, parameter :: WP = selected_real_kind(15,307)
    integer ndim                         ! problem dimension
    integer ntot                         ! grid point number for all axis
    integer nst                          ! grid point number for storage of analit. propagated functions
    integer nx(*),nr                ! grid point number for one axis
    integer :: nc,i
    integer test_wf
    integer flag                         ! remember for next step when it will be even (see test_wf)
    !c!modif CL 21/05/2009
    integer vmin,vmax,iv
    !c!fin modif
    real*8 m,tt                             ! reduced mass
    real*8 omega,tau,e0                  ! w
    real*8 eps                           ! max of wf on the right side of the spatial grid
    real*8 re,erot                       ! max of wf on the right side of the spatial grid
    real*8 dt,dtfs,tmax,dt2              ! time step value (au, fs) max time value ,dt*2 (for analit propagation)
    real*8 cm,ct,cv,pi                   ! mass: 12C --.gt. a.u.m, time: fs ---.gt. a.u.t, energy cm-1--.gt.a.u.,energy cm-1 --.gt..gt.ev
    real*8 rmin,rmax,ptl1(ntotal)
    real*8 x(nn,nxmax),x0(nn)            ! Jacobi grid
    real*8 r(nxmax)
    real*8 p(2*nxmax)                    ! impulse grid
    real*8 dx,dp,de,fc                   ! grid steps
    real*8 rrs(4096)                   ! rR*sqrt(sin(th))
    real*8 ptl(ncm,ntotal)               ! operators of diabatic pot. en.
    real*8 mu(ntotal)                    ! mu(x)
    real*8 sm(ntotal)                    ! cutoff function
    real*8 wfv(0:30,ntotal)                    ! eigenfunctions
    real*8 vec2(ntotal),vec(ntotal)                    ! cutoff function
    real*8 c(ntmax)                      ! field contour=E(t)
    real*8 tc(ntmax)                     ! time*contour=E(t)*t
    real*8 ta(20), ac(20)                ! time to modify wf = wf + coef(ia)*wf0, intensities atto
    real*8 field(ntmax)                  ! field=E(t)*cos(wt)
    real*8 ev(0:20),eviv
    !c!modif CL 17/03/2009
    real*8 evivh2
    real*8 vech2(ntotal) 		     ! initial state from H2
    real*8 projr(ntotal),proji(ntotal)   ! vectors for the projection of the propagated state onto the initial state
    real*8 g0,g1
    real*8 g0u,g1u
    real*8 timeField(ntmax)
    real*8 rinormr,rinormi
    real*8 ranormr,ranormi
    real*8 rinormru,rinormiu,rinormu
    real*8 ranormru,ranormiu,ranormu
    real*8 f1,f2
    real*8 vecr(ntotal),veci(ntotal)
    real*8 vecr2(ntotal),veci2(ntotal)
    !c! fin modif
    real*8 t,d(ntmax),td(ntmax)
    real*8 e,dist                        ! temporal
    real*8 rnorm,rinorm,rinorm1,ranorm,ranorm1
    real*8 calc_norm
    real*8 atto_norm_coef
    real*8 wfa1(2*ntotal),wfa2(2*ntotal),fa1,fa2
    complex*16 wf0(ncm,ntotal)           ! initial state wave function
!    complex(WP) :: wf(ncm,ntotal)            ! propagated on the final potential
    integer, parameter :: WP = selected_real_kind(15,307) 
    complex(WP) :: wf(ncm,nx(1))            ! propagated on the final potential

    complex*16 wfast(ncm,4096)       ! array for storage of analit. propagation results ! to be allocated to 4*npts
    complex*16 ekrr(ntotal),ekth(ntotal) ! exp operators of kin. en. from Rr, th
    complex*16 eptl(ncm,ncm,ntotal)      ! exp operators of diabatic pot. en.
    complex*16 evt(ntotal)               ! exp operators of pot. en. + exp (vt)
    complex*16 wf1(ntotal),wf01(ntotal)  ! wf,wf0 for one channel
    complex*16 podint(ntotal)            ! temporal to calculate integral
    complex*16 fa(2)
    complex*16 t1,t2                     ! temporal
    character*20 fn
    character*20 tmp
    real*8 arg,tf
    real*8 filon_cos,simpson
    real*8 rtmp1,rtmp2,rtmp3
    real*8 coeff
    complex*16 a(nxmax)               ! operators of diabatic pot. en.
    complex*16 b(nxmax)               ! operators of diabatic pot. en.
    common /ab/ a,b

    complex*16 zi
    parameter (zi=(0.D0,1.D0))
    parameter (coeff=5.33807915d-9)     ! 1.17d-3*cv

    integer p_th_in, p_th_out          ! FFTW theta
    integer p_Rr_in, p_Rr_out          ! FFTW Rr
    integer p_R_in , p_R_out           ! FFTW R    ! 1D
    integer p_Rnst_in , p_Rnst_out     ! FFTW R    ! 1D - in analit. propagation
        
    common /delta/d,d1,d2
        
    common /plan_th/p_th_in,p_th_out
    common /plan_Rr/p_Rr_in,p_Rr_out
    common /plan_R/ p_R_in ,p_R_out    ! for 1D
    common /plan_Rnst/ p_Rnst_in ,p_Rnst_out    ! for 1D
    common /field/ tau,E0

    integer :: nmax,na,ia,iwf,nt,j,jj,jr,ic,ntf,itmp,it,k,in,nv,kv,ihv,ncall
    integer :: nrr,nth,rank
    real*8,dimension(4096) :: energ,newkin,king,kinu
    real*8 :: meanr(*)
    !c ========================= input ==============================================
    write(*,*) 'ntotal',ntotal
    eps=1.D-7
    pi=dacos(-1.D0)
    re=2.D0
	
    open (8,file='units.cft')
    read (8,*) cm
    read (8,*) cv,ct
    close (8)

    open (8,file='at_mass.inp')
    read (8,*) m
    close (8)
    m=m*cm
	
    open (8,file='n_v.inp')
    read (8,*) vmin
    read (8,*) vmax
    read (8,*) nmax
    close (8)
	
!    open (8,file='atto.input')
!    read(8,*) na
!    do ia=1,na
!        read (8,*) ta(ia), ac(ia)  ! time to modify wf = wf + coef(ia)*wf0
!    end do
!    close (8)
!    write (*,*) 'input atto - ok'
	
	
    open (8,file='dynamics.inp')
    read (8,*) tmax,dt
    read (8,*) nc
    read (8,*) iwf
    close (8)
    nt=dint(tmax/dt)
    dtfs=dt
    dt=dt*ct
    tmax=tmax*ct

    write (*,*) 'input txt - ok'


    rmin=x(1,1)
    rmax=x(1,nx(1))
    dx=x(1,2)-x(1,1)
	
    nr=ntot
    do jr=1,nr
        r(jr)=x(1,jr)
    end do
	

    write (*,*) 'input grid - ok'

    nst=4096
    !nst=4*ntot



    write (*,*) 'input oper - ok'

    open (8, file='mu.unf',form='unformatted')
    do j=1,ntot
        read (8) mu(j)          ! in a.u.
    end do
    close (8)
    write (*,*) 'input mu - ok'


    open (8, file='smooth.unf',form='unformatted')
    read (8) (sm(j),j=1,ntot)          ! in a.u.
    close (8)
    write (*,*) 'input smooth - ok'

    open (8, file='field.unf',form='unformatted')
    read(8) ntf
    do j=1,ntf
        !c! modif CL 17/03/2009
        !c            read (8) field(j)      ! E(t)*cos(\omega*t)  in a.u.
        read (8) timeField(j),field(j)
    !c! fin modif
    end do
    close (8)

    open (8, file='field_cont.unf',form='unformatted')
    read(8) ntf
    read(8) omega
    do j=1,ntf
        read (8) c(j)               ! E(t)  in a.u.
        tc(j)=(j-0.5)*dt*c(j)              ! t*E(t)  in a.u.
    end do
    close (8)
	
    write (*,*) 'input field - ok'
	
    write(*,*) 'vmax=',vmax
    open (8,file='spectr.input')
    do iv=0,vmax
        read (8,*) itmp,ev(iv)
        ev(iv)=ev(iv)*cv
    end do
    close (8)


    write (*,*) 'input - ok'
    write (*,*) 'total time steps ', nt
    write (*,*) 'pulse time steps ', ntf

!    write(*,*) 'NXMAX!!!!!!!!!!!!!!!!!!!!!!!!!!',nxmax
!    write(*,*) 'NTOT!!!!!!!!!!!!!!!!!!!!!!!!!!',ntot
    !c ==================== inition ================================================
    !c ---------- FFTW_inition  -------------------------
    call FFTW_init(ndim,nx,nst)
    write (*,*) 'FFTW_init - ok'
    !c ---------- Delta(it) for pair number of points only  -------------------------
    do it=1,ntf,2
        d ((it+1)/2)=0.5D0 * filon_cos(c,omega,dt,it,0.D0)
        td((it+1)/2)=0.5D0 * filon_cos(tc,omega,dt,it,0.D0)
        !if (mod(it+1,1000).eq.0) write (*,*) 'Deltas ',it
    end do
    write (*,*) 'Deltas - ok'
	
    !c ---------- grid in implulse space  -------------------------
    dx=x(1,2)-x(1,1)
    write(*,*) 'dx',dx
    dp=2.d0*pi/dx/nst
    write(*,*) 'dp',dp
    do k=1,nst/2+1             ! inverse for fourn (see Num.Rec.)
        p(k)=(k-1)*dp
    end do
    do k=nst/2+2,nst
        p(k)=(k-nst-1)*dp
    end do
    open (81,file='grid_end_reached.dat')
    open (82,file='test_v_res.dat')
	
    if (iwf.ne.0) then
        write (*,*) 'wf of H2, the values iv=0 and in=0 will be used'
        vmin=0
        vmax=0
        nmax=0
    end if
	
    !c ---------- for prepare_eptl  -------------------------
    do in=0,nmax  ! ####################################################################################################"
	
        !c	write (*,*) ' #########################   iN=',in,' ##########################'
        erot=1.D0/2.D0/m/re**2*dfloat(in*(in+1))
        write (*,*) 'ev_N=',erot/cv
	   
        do j=1,ntot
            erot=1.D0/2.D0/m/x(1,j)**2*dfloat(in*(in+1))
            a(j)=-zi*0.5D0*dt*(ptl(1,j)+ptl(2,j)+2.D0*erot)
            b(j)=-zi*0.5D0*dt*(ptl(1,j)-ptl(2,j))
            ptl1(j)=ptl(1,j)+erot
        end do
        write(*,*) "test!!!!!!" , nx(1),ntot
        write(*,*) "calling prepare wfv"
        call prepare_wfv(wfv,cv,ptl1,M,RMIN,RMAX,dx,erot,ntot,nv)   ! pour le test sur la decomposition du wf sur les eigenfunctions
	
	
        do iv=vmin,vmax  ! +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
            !c	write (*,*) '_______________________________________________________________'
            write (*,*) 'iv=',iv
	
            open (11,file='norm_i_'//char(iv+48)//'.dat')
            open (12,file='norm_a_'//char(iv+48)//'.dat')
            open (13,file='norm_c_'//char(iv+48)//'.dat')



            do j=1,nst
                do ic=1,nc
                    wfast(ic,j)=dcmplx(0.D0,0.D0)
                end do
            end do

	
            !c ---------- calc of wave packets  -------------------------

	
            if (iwf.eq.0) then
                write (*,*) 'ev_0=',ev(iv)/cv
                erot=1.D0/2.D0/m/re**2*dfloat(in*(in+1))
                eviv=ev(iv)+erot
                !	  write (*,*) 'ev_N=',eviv/cv
                !      write(*,*) "TESTTTTT" , nx(1),ntot
                !      write(*,*) "calling getpsi for iv =" , iv
                call getpsi(ptl1,M,RMIN,RMAX,dx,eviv,ntot,VEC,ntotal,kv,cv)
                do j=1,ntot
                    vec(j)=vec(j)*(-1.D0)**iv
                end do
                write (*,*) 'ev_wf=',eviv/cv
                !c!modif CL 17/03/2009
                open (8, file='wf.unf',form='unformatted')
                read(8) ihv,evivh2
                do j=1,ntot
                    read (8) vech2(j)
                end do
                close (8)
            !c!fin modif

            else
                open (8, file='wf.unf',form='unformatted')
                read(8) ihv,eviv
                do j=1,ntot
                    read (8) vec(j)
                end do
                close (8)
                write (*,*) 'H2 v=',ihv,' egienfunction to be used','eviv',eviv
            end if

            call mult_rara(1,ntot,vec,vec,vec2)
            rinorm=sqrt(simpson(vec2,1,ntot,dx))
            write (*,*) 'before',iv,rinorm

            do j=1,ntot
                vec(j)=vec(j) / rinorm   * (-1.D0)**iv
                write(26,*) x(1,j),vec(j)**2
            end do
            call mult_rara(1,ntot,vec,vec,vec2)
            rinorm=sqrt(simpson(vec2,1,ntot,dx))
            write (*,*) 'after',iv,rinorm

	  
            write (*,*) 'wf ok'
	
            do j=1,ntot
                wf0(1,j)=dcmplx(vec(j),0.D0)
                wf0(2,j)=dcmplx(0.D0,0.D0)
                wf(1,j) =dcmplx(0.D0,0.D0)
                wf(2,j) =dcmplx(0.D0,0.D0)
            end do
            write (*,*) 'pulse duration ',(ntf-1)*dt/ct
	
            write (81,*) 'iN=',in,' iv=',iv
	
	
            ncall=0     ! number of call of analit propagation subroutine
            ia=1        ! number of atto
            !       write(*,*) "CALLING TEST_V"
            call test_v(wf0,wfv,x,dx,nx,ntot,nv,nc)

            !c ============== dynamics calculation ================================
            !c        goto 100
            do 100 it=1,nt
                !if (mod(it,1000).eq.0) write(*,'("ITERATION ON TIME ",I9.9,"/",I9.9)') it,nt
!                write(850+rank,'("ITERATION ON TIME ",I9.9,"/",I9.9)') it,nt

     
                if (it.le.ntf) call prepare_eptl(it,dt,ndim,nc,ntot,ptl,mu,field,eptl)
                !      write(*,*) "end of prepare_eptl"
                !write(*,*)it,field(it)
                if ((ia.le.na).and.((it-1)*dtfs.ge.ta(ia))) then
                    write (*,*) it,' atto ',ia,' # ',na ,' # ',(it-1)*dtfs,' # ',ta(ia),ac(ia)
                    do ic=1,nc
                        do j=1,ntot
                            wf(ic,j)=wf(ic,j) + &
                                wf0(ic,j)*dsqrt(ac(ia)) &
                                *exp(-zi*eviv*(it-1)*dt) &
                                *exp(-zi*ia*pi)        !20/08/2007 C.L. phase shift bw 2 atto
                        end do
                    end do
                    rinorm=calc_norm(ntot,nc,wf,dx)
                    write(*,*) 'Norm after ia=',ia,'=',rinorm
                    ia=ia+1                                 ! counter of wf modifications
                end if
!                write(*,*) 'This is a test'
!                if ((it.eq.1).or.((mod(it,100).eq.0).and.(it.le.1000))) then
!                    do j=1,nr
!                        write(500+it,*)j,abs(wf(1,j))**2,abs(wf(2,j))**2
!                    end do
!                end if
                !        write(*,*) 'ntot', ntot
!                WRITE(850+rank,*) 'BEGIN TIMESTEP NODE',rank
                call time_step1D (ntot,ndim,nc,nx,wf,eptl,ekrr)
!                write(*,*) 'wf(1,1):',wf(1,1)
!                write(*,*) 'wf(1,512):',wf(1,512)
!                write(*,*) 'wf(1,1024):',wf(1,1024)
                call rmoyen(wf,nc,nx,x,meanr(it))
!                WRITE(850+rank,*) 'END TIMESTEP NODE',rank

                !write(*,*)"look at fort.500+",it," and press any key"
                !read(*,*) tmp


                ires_test=test_wf(it,wf,ntot,nx,nc,eps,flag)
                if (ires_test.ne.0) then
                    !c           if (mod(it,100).eq.0) then
!                    WRITE(850+rank,*) 'Calling analit NODE',rank
                    call analit_prop(d,td,nt,it,dt,tmax,wf,sm,ntot,nc,nx,p,x,m,wfast,nst,pi,ncall,ntf)
!                    WRITE(850+rank,*) 'END analit NODE',rank
                    ncall=ncall+1  !number of calls
                    !write (*,*) 'divide_wf - ok, it=',it, ires_test
                end if




                !c          if (mod(it,1000).eq.0) write (*,*) it
                if (mod(it,1000).eq.0) call test_v(wf,wfv,x,dx,nx,ntot,nv,nc)

                !if (mod(it,1000).eq.0) write(*,'("END ITERATION ON TIME ",I9.9,"/",I9.9)') it,nt
!                write(850+rank,'("END ITERATION ON TIME ",I9.9,"/",I9.9)') it,nt
100         continue
            write (*,*) ncall
            write(*,*) "END OF TIME ITERATION"
            !c ================================ norm checking ===================================
            call analit_prop(d,td,nt,it,dt,tmax,wf,sm,ntot,nc,nx,p,x,m, wfast,nst,pi,ncall,ntf)
 
            write(*,*) "END OF analit_prop"
                    ! after the last step
            !c 	call results_output_ke (iv,in,ncall,nt,nt,nst,p,wfast,
            !c     &                       1.D0,dtfs,rrs,m)
            write(*,*) '1: iat',iat,'iv',iv,'in',in

            call results_output (iv,in,ncall,nt,nt,nst,p,wfast,1.d0,dtfs,rrs,newkin,king,kinu,energ,iat)
!            do i=1,4096
!              write(577,*) energ(i),newkin(i)
!            end do
            write(*,*) "END OF results_output"
            rinorm=calc_norm(ntot,nc,wf,dx)
            ranorm=calc_norm(nst,nc,wfast,dp)
	
	
            write (*,*) 'norm of the interacting part=', rinorm
            write (*,*) 'total photodissociation probability = ', ranorm
            
            write (*,*) 'norm of the final function=', rinorm+ranorm

96          write (*,*) 'output - ok'
            write (*,*) 'dynamics - ok'

        end do ! +++++++++++++++++++++++++++++++++++  iN
    end do  ! ##################################  iv

    close(81)
    close(82)
       
99  format (i7,'/',i7,'  completed   ',e13.6,'  ',e13.6)
  do i=1,4096
    write(550+rank,*) energ(i),newkin(i)
  end do
return
end subroutine dyn
!c ========================================================================================      
subroutine time_step1D (ntot,ndim,nc,nx,wf,eptl,ekrr)
        include 'limits.h'
!    integer, parameter :: WP = selected_real_kind(15,307)
    integer ndim              ! problem dimension
    integer ntot              ! grid point number for all axis
    integer nx(nn),nr(2)   ! grid point numbers for one axis
!    complex(WP) :: wf(ncm,ntotal)       ! propagated on the final potential
    complex*16 :: wf(ncm,ntotal)       ! propagated on the final potential
    complex*16 ekrr(ntotal),ekth(ntotal) ! exp operators of kin. en. from Rr, th
    complex*16 eptl(ncm,ncm,ntotal)       ! exp operators of pot. en. + exp (vt)
    complex*16 evt(ntotal)       ! exp operators of pot. en. + exp (vt)
!    complex(WP) :: f(ncm,2*ntotal)         ! tmp
    complex*16 :: f(ncm,2*ntotal)         ! tmp

    nr(1)=nx(1)
    nrr=nx(1)
    nr(2)=1
    nth=0
    !        write(*,*) "TEST30"
    call mult_eptl (wf,eptl,ntot,nc)
    !        write(*,*) "end of mult_eptl ntot=",ntot
    call fft (wf,f,nrr,nth,nr,1,ntot,nc,'R1')    ! here we works with wf. (f is tmp)
    !        write(*,*) "end of fft +1"
    call mult_ekin (wf,ekrr,ntot,nc)
    !        write(*,*) "end of mult_ekin"
    call fft (wf,f,nrr,nth,nr,-1,ntot,nc,'R1')
    !        write(*,*) "end of fft -1"
    call mult_eptl (wf,eptl,ntot,nc)
!       write(*,*) "TEST40"

end subroutine time_step1D
!c ====================================================================================
subroutine prepare_eptl(it,dt,ndim,nc,ntot,ptl,mu,field,eptl)
	
        include 'limits.h'
    integer ntot,nx(nn),j                     ! grid point number for all axis
    real*8 x(nn,nxmax)
    real*8 field(ntmax)              ! field=E(t)*cos(wt)
    real*8 mu(ntotal)                ! mu(x)
    real*8 dt,costh
    real*8 ptl(ncm,ntotal)               ! operators of diabatic pot. en.
    complex*16 eptl(ncm,ncm,ntotal)  ! exp operators of diabatic pot. en.
    complex*16 c,v1,w12
    complex*16 zi,z0
    parameter (zi=(0.D0,1.D0))
    parameter (z0=(0.D0,0.D0))

    complex*16 a1,b1
    complex*16 a(nxmax)               ! operators of diabatic pot. en.
    complex*16 b(nxmax)               ! operators of diabatic pot. en.
    common /ab/ a,b

    do jr=1,ntot
        w12=zi*0.5D0*dt*mu(jr)*field(it)        ! adiag element of potential matrix
        c=sqrt(b(jr)**2+4.D0*w12**2)
        a1=exp(0.5D0*(a(jr)+c))
        b1=exp(0.5D0*(a(jr)-c))
        if (abs(c).ne.0.D0) then
            eptl(1,1,jr)=  0.5D0*(b1*(-b(jr)+c)+a1*(b(jr)+c))/c
            eptl(2,2,jr)=  0.5D0*(b1*( b(jr)+c)-a1*(b(jr)-c))/c
            eptl(1,2,jr)=  w12*(a1-b1)/c
            eptl(2,1,jr)= eptl(1,2,jr)
        else
            v1=0.5D0*(a(jr)+b(jr))
            v2=0.5D0*(a(jr)-b(jr))
            eptl(1,1,jr)= exp(v1)
            eptl(2,2,jr)= exp(v2)
            eptl(1,2,jr)= z0
            eptl(2,1,jr)= eptl(1,2,jr)
        end if
    end do
end subroutine prepare_eptl
	
!c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
real*8 function calc_norm(nr,nc,wf,dx)

        include 'limits.h'
    integer nr                     ! grid point number for all axis
    real*8 dx
    real*8 wfa1(ntotal),wfa2(ntotal),f1,f2
    real*8 simpson
    complex*16 wf(ncm,ntotal)        ! propagated on the final potential

    do i=1,nr
        wfa1(i)=abs(wf(1,i))**2
        wfa2(i)=abs(wf(2,i))**2
    end do
    f1=simpson(wfa1,1,nr,dx)
    f2=simpson(wfa2,1,nr,dx)
    calc_norm=f1+f2

end function calc_norm
	
      
!c $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$	
subroutine rmoyen(wf,nc,nx,x,meanr)
  integer, parameter :: WP = selected_real_kind(15,307) 
  integer :: nc,nx(*),i
  complex(WP) :: wf(nc,nx(1))            ! propagated on the final potential
  real*8 :: tmp(nx(1))            ! propagated on the final potential
  real*8 :: x(nc,nx(1)),meanr            ! Jacobi grid
  do i=1,nx(1)
  ! integrale psi x psi
  tmp(i)=x(1,i)*abs(wf(1,i))**2
!  write(*,*) 'tmp(',i,'):',tmp(i),wf(1,i)
 ! if ((i.eq.1).or.(i.eq.512).or.(i.eq.1024)) then
 !   write(*,*) 'i,tmp:',i,tmp(i)
 ! end if
  end do
  call INT_R1D(x(1,2)-x(1,1),tmp,meanr,nx(1))
  !write(*,*) 'meanr:',meanr
end subroutine rmoyen


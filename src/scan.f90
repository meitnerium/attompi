! v0.1.0 thu mar 20 10:45:55 edt 2014
! v0.1.0 dyn.f90 scan.f90 ajout de l'?criture de rmoyen
! v0.0.9 dyn.f90 numerov.f modification pour utiliser ptl(ncm,npoints) pour ne
! pas cr?er un nouveau vecteur thu mar 20 10:45:55 edt 2014
!v0.0.8 status var changed to stat on some mpissend/recv command thu mar 20 10:45:55 edt 2014
!v0.0.7 field.f90 modification de la periode 
!v0.0.6 : scan.f90 tap.dat a ?t? refait (calcul? pour que la derniere valeur soit en premier) Wed Mar 19 16:49:10 EDT 2014
!v0.0.5 : scan.f90 dyn.f90 le timing fonctionne normalement
!v0.0.4 : scan.f90 dyn.f90 Changer tout les timing pour le timing MPI Wed Mar 19 13:19:10 EDT 2014
!v0.0.3 : scan.f90 Ajout des calculs parrall?le des psiv Wed Mar 19 10:10:07 EDT 2014
!v0.0.2 : scan.f90 allocate tag Wed Mar 19 10:10:07 EDT 2014
!V0.0.1 : scan.f90 Ajout de na et pulseuvtd dans la lecture de scan.inp Tue Mar 18 15:35:14 EDT 2014
!TODO enlever tarss
! verifier l ecriture des fort.???
! ecrire le rmoyen
! tester les ierr
! enlever les ntotal pour allocate 
! total photodissociation probability =             NaN
! tap.dat??? et la variable tap, sert a quoi ???
!forrtl: warning (402): fort: (1): In call to GETPSI, an array temporary was created for argument #1
!desallocate 
!
program scan
    ! USE sub
    USE mpi
  include 'limits.h'
    INTEGER err, rank, size
    integer, dimension(:),allocatable :: tag
    real*8,  dimension(:),allocatable :: taranorm
    real*8  :: tamin,tamax,tap,tadt
    integer :: i,nat,lnt,iat,j,itmp,jv

    !c  
    !c   program for solution of the 1D 2 channel Schrodinger equation 
    !c   with the time dependent Hamiltonian by a split
    !c   operator metod. This program propagate wafe function of the
    !c   inition state on the both (initial and final) potential surfaces.
    !c
        
!    integer, parameter :: WP = selected_real_kind(15,307)
    integer ndim,rc                         ! problem dimension
    integer ntot                         ! grid point number for all axis
    integer nst                          ! grid point number for storage of analit. propagated functions
    integer nx(nn),nr                ! grid point number for one axis
    integer test_wf
    integer flag                         ! remember for next step when it will be even (see test_wf)
    !c!modif CL 21/05/2009
    integer vmin,vmax,iv
    !c!fin modif
    real*8 m,tt                             ! reduced mass
    real*8 omega,tau,e0                  ! w
    real*8 eps                           ! max of wf on the right side of the spatial grid
    real*8 re,erot                       ! max of wf on the right side of the spatial grid
    real*8 dt,dtfs,tmax,dt2              ! time step value (au, fs) max time value ,dt*2 (for analit propagation)
    real*8 cm,ct,cv,pi                   ! mass: 12C --.gt. a.u.m, time: fs ---.gt. a.u.t, energy cm-1--.gt.a.u.,energy cm-1 --.gt..gt.ev
    real*8 rmin,rmax,ptl1(ncm,ntotal)
    real*8 x(nn,nxmax),x0(nn)            ! Jacobi grid
    real*8 r(nxmax)
    real*8 p(2*nxmax)                    ! impulse grid
    real*8 dx,dp,de,fc                   ! grid steps
    real*8 rrs(ntotal)                   ! rR*sqrt(sin(th))
    real*8 rrstmp(ntotal)                   ! rR*sqrt(sin(th))
    real*8,allocatable ::  tarrs(:,:)                   ! rR*sqrt(sin(th))
    real*8 ptl(ncm,ntotal)               ! operators of diabatic pot. en.
    real*8 mu(ntotal)                    ! mu(x)
    real*8 sm(ntotal)                    ! cutoff function
    real*8 wfv(0:30,ntotal)                    ! eigenfunctions
    real*8 vec2(ntotal),vec(ntotal)                    ! cutoff function
    real*8 c(ntmax)                      ! field contour=E(t)
    real*8 tc(ntmax)                     ! time*contour=E(t)*t
    real*8 ta(20), ac(20)                ! time to modify wf = wf + coef(ia)*wf0, intensities atto
    real*8 field(ntmax)                  ! field=E(t)*cos(wt)
    real*8 ev(0:20),eviv
    !c!modif CL 17/03/2009
    real*8 evivh2
    real*8 vech2(ntotal)                 ! initial state from H2
    real*8 projr(ntotal),proji(ntotal)   ! vectors for the projection of the propagated state onto the initial state 
    real*8 :: projvnt(ntotal)
    real*8 g0,g1
    real*8 g0u,g1u
    real*8 timeField(ntmax)
    real*8 rinormr,rinormi
    real*8 ranormr,ranormi
    real*8 rinormru,rinormiu,rinormu
    real*8 ranormru,ranormiu,ranormu
    real*8 f1,f2
    real*8 vecr(ntotal),veci(ntotal)
    real*8 vecr2(ntotal),veci2(ntotal)
    !c! fin modif
    real*8 t,d(ntmax),td(ntmax)
    real*8 e,dist                        ! temporal
    real*8 rnorm,rinorm,rinorm1,ranorm,ranorm1
    !   real*8 calc_norm
    real*8 atto_norm_coef
    real*8 fa1,fa2
!    complex(WP) :: wf(ncm,ntotal)            ! propagated on the final potential
    complex*16 :: wf(ncm,ntotal)            ! propagated on the final potential
    complex*16 wfast(ncm,2*ntotal)       ! array for storage of analit. propagation results
    complex*16 ekrr(ntotal),ekth(ntotal) ! exp operators of kin. en. from Rr, th
    complex*16 eptl(ncm,ncm,ntotal)      ! exp operators of diabatic pot. en.
    complex*16 evt(ntotal)               ! exp operators of pot. en. + exp (vt)
    complex*16 wf1(ntotal),wf01(ntotal)  ! wf,wf0 for one channel
    complex*16 podint(ntotal)            ! temporal to calculate integral
    complex*16 fa(2)
    complex*16 t1,t2                     ! temporal 
    character*20 fn
    character*20 tmp
    real*8 arg,tf
    real*8 filon_cos,simpson
    real*8 rtmp1,rtmp2,rtmp3
    real*8 coeff
    complex*16 a(nxmax)               ! operators of diabatic pot. en.
    complex*16 b(nxmax)               ! operators of diabatic pot. en.
    common /ab/ a,b

    complex*16 zi
    parameter (zi=(0.D0,1.D0))
    parameter (coeff=5.33807915d-9)     ! 1.17d-3*cv
    character*200 :: texto
    integer p_th_in, p_th_out          ! FFTW theta
    integer p_Rr_in, p_Rr_out          ! FFTW Rr
    integer p_R_in , p_R_out           ! FFTW R    ! 1D
    integer p_Rnst_in , p_Rnst_out     ! FFTW R    ! 1D - in analit. propagation
      
    common /delta/d,d1,d2
       
    common /plan_th/p_th_in,p_th_out
    common /plan_Rr/p_Rr_in,p_Rr_out
    common /plan_R/ p_R_in ,p_R_out    ! for 1D
    common /plan_Rnst/ p_Rnst_in ,p_Rnst_out    ! for 1D
    common /field/ tau,E0
    real*8 :: vecv(ntotal),tp
    integer nv,inv,invrank
    real :: start, finish
    integer stat(MPI_STATUS_SIZE)
    real*8, allocatable :: tpvec(:)
    character*200 :: unformat,unformat2
    real*8 :: cev,realtmp
    integer :: intmp,inttmp
    INTEGER :: kv
    REAL :: rate
    real*8 :: pulseuvtd,meanta
    real*8,dimension(4096) :: energ,kin,newkin,energ2,newkin2,king,kinu
    real*8,allocatable :: takin(:,:),taenerg(:,:),taking(:,:),takinu(:,:) !taenerg inutile
    real*8,allocatable :: meanr(:),meanrat(:,:)
    real*8 :: r0,atmass,rot1,rot2
    real*8 :: lambda,sigma,rsmooth
    integer :: nc
    integer :: nmax,ftype,nsin
    real*8 :: time1,time2,time3,totwait,totwait2

    namelist /datos/ ndim,c,nc,r0,rmin,rmax,nx,tmax,dt,lambda &
        ,field,tau,sigma,rsmooth,atmass &
        ,rot1,rot2,vmin,vmax,nmax,e0,wf,tp,tamin,tamax,pulseuvdt,tadt,na &
        ,ftype,nsin

    CALL MPI_Init(ierr)
    CALL MPI_Comm_rank (MPI_COMM_WORLD, rank, ierr)
    CALL MPI_Comm_size (MPI_COMM_WORLD, size, ierr)
    time1 = MPI_WTIME()
    totwait=0.d0



    open (8,file='units.cft')
    read (8,*) cm
    read (8,*) cv,ct
    close (8)

    open (8,file='at_mass.inp')
    read (8,*) m
    close (8)
    m=m*cm
	
    open (8,file='n_v.inp')
    read (8,*) vmin
    read (8,*) vmax
    read (8,*) nmax
    close (8)
	
!    open (8,file='atto.input')
!    read(8,*) na
!    do ia=1,na
!        read (8,*) ta(ia), ac(ia)  ! time to modify wf = wf + coef(ia)*wf0
!    end do
!    close (8)
!    write (*,*) 'input atto - ok'
	
	
    open (8,file='dynamics.inp')
    read (8,*) tmax,dt
    read (8,*) nc
    read (8,*) iwf
    close (8)
    nt=dint(tmax/dt)
    dtfs=dt
    dt=dt*ct
    tmax=tmax*ct

    write (*,*) 'input txt - ok'



    open (8,file='0_data.input')
    read(8,nml=datos)
    close (8)
    open (8,file='grid.unf',form='unformatted')
    read (8) ndim,ntot,(nx(j),j=1,ndim)
    do j=1,ndim
        do jj=1,nx(j)
            read (8) x(j,jj)
        end do
    end do
    close (8)
    open (8, file='cmplx_oper.unf',form='unformatted')
    do j=1,ntot
        read (8) ekrr(j),(ptl(ic,j),ic=1,nc)
    end do
    close (8)
    write(*,*)"RANK=",rank 
    if (rank.eq.0) then
      open(18,name='newkin2d2.dat',status='unknown')
      open(25,name='newkin2dg.dat',status='unknown')
      open(20,name='newkin2du.dat',status='unknown')
      open(22,name='meanr.dat',status='unknown')
    end if
    cev=27.212d0
    time1 = MPI_WTIME()
    allocate(tag(124))

    nat=(tamax-tamin)/tadt+1
!    write(*,*) 'NAT=',nat,tamin,tamax
    if (rank.eq.0) then
      allocate(takin(4096,size))
      allocate(taking(4096,size))
      allocate(takinu(4096,size))
      allocate(taranorm(nat))
      allocate(meanrat(nt,nat))
    end if
    allocate(meanr(nt))
    lnt=nat/size+1
!    write(*,*) 'lnt=',lnt
    !    do j=1,nat
    !      tpvec(j)=(tp-tamin-(nat-j-1)*tadt)
    !    end do
    do i=1,lnt 
        iat=size*(i-1)+rank+1
        ta(1)=tamin+(iat-1)*tadt
        if (ta(1).le.tamax) then
            write(456,*) rank,iat,ta(1)
        end if
    end do
    do i=lnt,1,-1 
        iat=size*(i-1)+rank+1
        meanta=tamin+(iat-1)*tadt
        if (mod(na,2).eq.0) then
           ! les 2 do loop sont inversé !
            do inttmp=1,na
                ta(inttmp)=tamin+(dfloat(iat)-1)*tadt+((-dfloat(na)/2.d0+(dfloat(inttmp)-1)))*pulseuvdt
                ac(inttmp)=1.d0/(dfloat(na)) ! TO BE VERIFIED
                write(999,*)meanta,ta(inttmp),ac(inttmp)
            end do
        else
            do inttmp=1,na
                ta(inttmp)=tamin+(dfloat(iat)-1)*tadt+((-dfloat(na)/2.d0+(dfloat(inttmp)-1.d0))+0.5d0)*pulseuvdt
                ac(inttmp)=1.d0/(dfloat(na)) ! TO BE VERIFIED
                !write(999,*)meanta,ta(inttmp),ac(inttmp)
            end do
        end if
        !ta(1)=tamin+(iat-1)*tadt
        !      write(*,*) 'Doing ia=',ia,'ta=',ta(1),'(max=',tamax,') on rank=',rank
        if (tamin+dfloat((size*(i-1))+rank)*tadt.le.tamax) then ! changer pour tamean?
!            write(*,*) 'rank,iat,ta(1)',rank,iat,ta(1)


            !c ============== dynamics calculation ================================
            !c        goto 100
            !      write(*,*) "NST BEFORE DYN",nst
!            write(*,*) "CALLING DYN FOR iat=",iat

            call dyn(ta,iat,ac,na,energ,newkin,king,kinu,dtfs,tmax,ct,rank,ndim,ntot,nx,x,ekrr,ptl,ranorm,meanr) ! definir les out et les in
            call replacek(energ,newkin,newkin2,energ2,king,kinu)

            time2 = MPI_WTIME()
!            write(*,*) 'Calling MPI_BARRIER1 at time ', time2-time1
            call  MPI_Barrier(MPI_COMM_WORLD,ierr)
            time3 = MPI_WTIME()
!            write(*,*) 'End of MPI_BARRIER1,ierr,rank',ierr,rank
!            write(*,*) 'End of MPI_BARRIER1 after ',time3-time2,rank
            totwait=totwait+(time3-time2)
!            write(*,*) 'Total waited time on rank',rank,totwait
        !! FOR TESTING MPI SEND' RECV, TO BE COMMENTED OUT
        !      newkin=dfloat(iat)
          !do j=1,nt
          !  write(500+iat,*) (j-1)*dt,meanr(j)
          !  write(1000+iat,*) (j-1)*dt,projvnt(j)
          !end do
          !write(texto,*) 'Dyn Calc terminated for ta = ',ta(1)
        !     write(*,*) 'test in main rrs(500)',rrs(500)
          !if (rank.eq.0) then
           ! do j=1,size-1
           !   realtmp=tamin+((size*(i-1))+j)*tadt
              !if (realtmp.le.tamax) then
              !  write(*,*) 'Receiving texto from ',j
              !  rc=MPI_RECV(texto,200,MPI_CHARACTER,j,MPI_ANY_TAG,MPI_COMM_WORLD,stat,ierr)
              !  write(*,*) 'End Receiving texto from ',j
              !  time2 = MPI_WTIME()
              !  WRITE(*,998) texto, (time2 - time1)/(60.d0*60.d0), mod(((time2 - time1)),60.d0*60.d0)/60.d0, mod(((time2 - time1)),60.d0)  
        !998 format (A ,' after ' F3.0,' h ',F3.0,' m ',  F3.0 ,' s')
          !    end if
           ! end do 
          !else
          !  realtmp=tamin+((size*(i-1))+rank)*tadt
          !  if (realtmp.le.tamax) then
          !    write(*,*) 'Sending texto from ',rank
          !    rc=MPI_Send(texto,200,MPI_CHARACTER,0,MPI_ANY_TAG,MPI_COMM_WORLD,ierr)
          !    write(*,*) 'End Sending texto from ',rank
          !  end if
          !end if
        else
          !MPIBARRIER
            time2 = MPI_WTIME()
!            write(*,*) 'Calling MPI_BARRIER1 at time ', time2-time1
          call  MPI_Barrier(MPI_COMM_WORLD,ierr)
            time3 = MPI_WTIME()
!          write(*,*) 'End of MPI_BARRIER1,ierr,rank',ierr,rank
!            write(*,*) 'End of MPI_BARRIER1 after ',time3-time2,rank
            totwait=totwait+(time3-time2)
!            write(*,*) 'Total waited time on rank',rank,totwait

        end if
            ! les resultats sont dans le rrs, faut le retourner dans le rank 0
            ! (ntotal)
        !real*8,allocatable ::  tarrs(:,:)                   ! rR*sqrt(sin(th))
        if((rank.eq.0).and.(tamin+dfloat((size*(i-1)))*tadt.le.tamax)) then
            !      tarrs(iat,:)=rrs(:)
!            write(*,*) 'iat',iat,'nat-iat+1',nat-iat+1
            takin(:,1)=newkin2(:)  ! a l endroit, a l envers?
            taking(:,1)=king(:)  ! a l endroit, a l envers?
            takinu(:,1)=kinu(:)  ! a l endroit, a l envers?
            taranorm(iat)=ranorm 
            meanrat(iat,:)=meanr(:)

            !do k=1,4096
            !    write(650,*) energ2(k),takin(k,nat-iat+1)
            !end do
            !tarrs(iat,:)=energ2(:)
            do j=1,size-1
                realtmp=tamin+dfloat((size*(i-1))+j)*tadt
                if (realtmp.le.tamax) then
                    !intmp=size*(i-1)+j
                    !intmp=size*(i-1)+j+1
                    !write(*,*) "test j,inttmp",j,inttmp
                    !write(*,*) 'receving rss from node',j
                    !rc=MPI_RECV(rrstmp,ntotal,MPI_DOUBLE_PRECISION,j,j,MPI_COMM_WORLD,stat,ierr)
!                    write(*,*) 'Receiving inttmp from',j
                    call MPI_RECV(intmp,1,MPI_INTEGER,j,1,MPI_COMM_WORLD,stat,ierr)
!                    write(*,*) 'End Receiving inttmp from',j,ierr,inttmp
!                    write(*,*) 'Receiving newkin from',j
                    call MPI_RECV(newkin2,ntotal,MPI_DOUBLE_PRECISION,j,1,MPI_COMM_WORLD,stat,ierr)
                    call MPI_RECV(king,ntotal,MPI_DOUBLE_PRECISION,j,1,MPI_COMM_WORLD,stat,ierr)
                    call MPI_RECV(kinu,ntotal,MPI_DOUBLE_PRECISION,j,1,MPI_COMM_WORLD,stat,ierr)
!                    write(*,*) 'End Receiving newkin from',j,ierr
                    call MPI_RECV(ranorm,1,MPI_DOUBLE_PRECISION,j,1,MPI_COMM_WORLD,stat,ierr)
!                    write(1000+j,*) 'End Receiving ranorm from',j,ierr,ranorm
                    call MPI_RECV(meanr,nt,MPI_DOUBLE_PRECISION,j,j+2,MPI_COMM_WORLD,stat,ierr)
!                    write(1000+j,*) 'End Receiving meanr from',j,ierr
                    !rc=MPI_RECV(energ2,ntotal,MPI_DOUBLE_PRECISION,j,j+2,MPI_COMM_WORLD,stat,ierr)
                    !write(*,*) 'test for rrstmp(500) from j=',j,'and intmp=',intmp,'is=',rrstmp(500)
                    !tarrs(intmp,:)=rrstmp(:)
!                    write(*,*) 'intmp',intmp,'nat-intmp+1',nat-intmp+1
                    takin(:,j+1)=newkin2(:)
                    taking(:,j+1)=king(:)
                    takinu(:,j+1)=kinu(:)
                    taranorm(intmp)=ranorm
                    !do k=1,4096
                    !    write(650+j,*) energ2(k),takin(k,nat-intmp+1),newkin2(k)
                    !end do 
                    meanrat(:,inttmp)=meanr(:)
                  !taenerg(intmp,:)=energ2(:)
                end if
            end do
        else if ((rank.gt.0).and.((tamin+dfloat((size*(i-1))+rank)*tadt).le.tamax)) then
!            write(*,*) 'Sending rss from node',rank
            !rc=MPI_Send(rrs,ntotal,MPI_DOUBLE_PRECISION,0,rank,MPI_COMM_WORLD,ierr)
!            write(*,*) 'Sending inttmp from',rank
            call MPI_Send(iat,1,MPI_INTEGER,0,1,MPI_COMM_WORLD,ierr)
!            write(*,*) 'End Sending inttmp from',rank,ierr
!            write(*,*) 'Sending newkin from',rank
            call MPI_Send(newkin2,ntotal,MPI_DOUBLE_PRECISION,0,1,MPI_COMM_WORLD,ierr)
!            write(*,*) 'End Sending newkin from',rank,ierr
            call MPI_Send(king,ntotal,MPI_DOUBLE_PRECISION,0,1,MPI_COMM_WORLD,ierr)
!            write(*,*) 'End Sending king from',rank,ierr
            call MPI_Send(kinu,ntotal,MPI_DOUBLE_PRECISION,0,1,MPI_COMM_WORLD,ierr)
!            write(*,*) 'End Sending kinu from',rank,ierr
            call MPI_Send(ranorm,1,MPI_DOUBLE_PRECISION,0,1,MPI_COMM_WORLD,ierr)
!            write(1000+rank,*) 'End Sending ranorm from',rank,ierr,ranorm
              !write(*,*) 'intmp',intmp,'nat-intmp+1',nat-intmp+1
            call MPI_Send(meanr,nt,MPI_DOUBLE_PRECISION,0,rank+2,MPI_COMM_WORLD,ierr)
!            write(1000+rank,*) 'End Sending meanr from',rank,ierr
          !rc=MPI_Send(taenerg,ntotal,MPI_DOUBLE_PRECISION,0,rank+2,MPI_COMM_WORLD,ierr)
        end if
        !call MPI_Wait(MPI_REQUEST_NULL,stat,ierr)
            time2 = MPI_WTIME()
!        write(*,*) 'Calling MPI_BARRIER  at time ', time2-time1

        call  MPI_Barrier(MPI_COMM_WORLD,ierr)
            time3 = MPI_WTIME()
!        write(*,*) 'End Calling MPI_BARRIER'
!            write(*,*) 'End of MPI_BARRIER after ',time3-time2,rank
            totwait=totwait+(time3-time2)
!            write(*,*) 'Total waited time on rank',rank,totwait









      !open(18,name='newkin2d2.dat',status='unknown')
      !open(19,name='newkin2dg.dat',status='unknown')
      !open(20,name='newkin2du.dat',status='unknown')
      !open(22,name='meanr.dat',status='unknown')






        if (rank.eq.0) then
          write(unformat,*) '(4096(2x,E22.15))'
          write(unformat2,*) '(',nt,'(2x,E22.15))'
          do j=size,1,-1
            write(*,*) 'Writing on newkin2d2.dat for rank =',j
            realtmp=tamin+dfloat((size*(i-1))+j)*tadt
            if (realtmp.le.tamax+tadt) then
              write(998,*) 'writting on newkin2d2.dat for iat=',nat-(size*(i-1)+j)
              write(*,*) ''
              write(*,*) 'WRITTING on 18 : takin(:,',j,')'
              write(*,*) 'realtmp=',realtmp,'j=',j
              write(*,*) ''
              write(18,unformat) (takin(k,j),k=1,4096)
!              write(25,*) "testFDFDFD"
!              write(*,*) "testFDFDFD", j,taking(1,1)
              write(25,unformat) (taking(k,j),k=1,4096)
              write(20,unformat) (takinu(k,j),k=1,4096)
!              write(*,*) "before error!!!",size*(i-1)+size-1,nt
              write(22,unformat2) (meanrat(k,size*(i-1)+j),k=1,nt)
            end if
 

          end do

        end if


    write(*,*) 'TEST1, rank=',rank
    end do
    if (rank.eq.0) then
        write(unformat,*) '(',nat,'(2x,E22.15))'
        open(9,name='config.m',status='unknown')
        open(21,name='ranorm.dat',status='unknown')
        !open(10,name='newkin2d.dat',status='unknown')
        !    open(11,name='energ2d.dat',status='unknown')
        !    open(12,name='meanr.dat',status='unknown')
        open(13,name='t.dat',status='unknown')
        !do i=1,4096 !nst
            !write(9,unformat)(tarrs(j,i),j=1,nat)
        !    write(10,unformat)(takin(i,j),j=1,nat)
        !        write(11,unformat)(energ2(i),j=1,nat)
        !end do
        write(*,*) 'TEST2'
        !do i=1,4096 !nst
        !    do j=nat,1,-1
        !        write(11,*)  tp-(tamin+(j-1))*tadt,energ(i),takin(i,j)
        !    end do
        !    write(11,*) ''
        !end do
              do i=1,nt !nst
        !        write(12,unformat)(meanrat(j,i),j=1,nat)
                write(13,unformat)((i-1)*dtfs,j=1,nat)
              end do
        open(9,name='config.m',status='unknown')
        write(9,*) 'tamin=',tp-tamax,';'
        write(9,*) 'tamax=',tp-tamin,';'
        write(9,*) 'tadt=',tadt,';'
        write(9,*) 'Emin=', energ2(1),';'
        write(9,*) 'Emax=', energ2(4096),';'
        write(9,*) 'DE=', (energ2(2)-energ2(1)),';'
        write(9,*) 't0=0;'
        write(9,*) 'dt=',dtfs,';'
        write(9,*) 'tmax=', tmax/ct,';'
        write(9,*) 'rmin=', x(1,1),';'
        write(9,*) 'rmax=', x(1,nx(1)),';'
        write(9,*) 'delr=', x(1,2)-x(1,1),';'
        
            !do i=1,nst
            !  write(9,unformat) (tpvec(j),j=1,nat)
            !end do
 
        close(9)
        !close(10)
        close(11)
        close(18)
        close(25)
        do i=nat,1,-1
          write(21,*)taranorm(i)
        end do
        close(20)
        close(21)
        close(22)


        write(*,*) 'TEST3'


	! create gnuplot command file
!	OPEN(10,ACCESS='SEQUENTIAL',FILE='gp.txt')
!	write(10,*) 'set terminal png'
!	write(10,*) 'set style line 1 lc rgb "black" pt 7'
!	write(10,*) 'set output "field.png"'
!	write(10,*) 'set xlabel "Time (fs)"'
!	write(10,*) 'set ylabel "E (u.a.)"'
!	write(10,*) 'plot "field.dat" w l'
!	CLOSE(10,STATUS='KEEP')

!  ret=SYSTEM('gnuplot gp.txt')
!	ret=SYSTEM('rm gp.txt')








        !    close(12)
            close(13)
        time2 = MPI_WTIME()
        WRITE(*,999) (time2-time1)/(60.d0*60.d0), mod(((time2 - time1)),60.d0*60.d0)/60.d0, mod(((time2 - time1)),60.d0)  
999     format ('All finish after ', F3.0,' h ',F3.0,' m ',  F3.0 ,' s')
    end if
! REDUCE SEAM TO DOESNT WORK
!    call MPI_REDUCE(totwait, totwait2, 1, MPI_DOUBLE_PRECISION, MPI_SUM, 0, MPI_COMM_WORLD,stat,ierr) 
!    if (rank.eq.0) then
!      write(*,998) ,totwait2/(60.d0*60.d0), mod(((totwait2)),60.d0*60.d0)/60.d0, mod(((totwait2)),60.d0)
!998     format ("TOTAL WAITED TIME = ", F3.0,' h ',F3.0,' m ',  F3.0 ,' s')
!    end if
    ! 20    CONTINUE
    deallocate(tag)
    if (rank.eq.0) then
      deallocate(takin)
      deallocate(taking)
      deallocate(takinu)
      deallocate(taranorm)
    end if
    CALL MPI_Finalize(ierr)
end program scan


subroutine replacek(energ,kin,newkin,e,king,kinu)
  real*8 :: De,e(4096),energ(4096),kin(4096),newkin(4096),intens
  real*8 :: king(4096),kinu(4096)
  real*8 :: newking(4096),newkinu(4096)
  integer :: j,z,i,numiter
  dE=1.d-3
  z=1
!         do 100 i=numiter,1,-1
  do j=1,4096
    e(j)=(j-1)*dE
    do while(e(j).gt.energ(z+1)) 
      if (e(j).gt.energ(z+1)) then
        z=z+1
      end if
    end do
    if (e(j).lt.(energ(z))) then
      write(*,*) "Go to bed!"
    end if
    if (e(j).gt.(energ(z+1))) then
      write(*,*) "Problem here !"
    end if
    if (z.eq.4096) then
      e(j)=kin(z) ! c est pas plus intens???????????
    else
!       write(*,*)'z,j',z,j,energ(z+1),energ(z)
      intens=(kin(z+1)-kin(z))/(energ(z+1)-energ(z))*(e(j)-energ(z))+kin(z)
      newking(j)=(king(z+1)-king(z))/(energ(z+1)-energ(z))*(e(j)-energ(z))+king(z)
      newkinu(j)=(kinu(z+1)-kinu(z))/(energ(z+1)-energ(z))*(e(j)-energ(z))+kinu(z)
      if (intens.lt.0)then
        write(*,*)"Debug",z,i,j,kin(z),kin(z+1),e(j),energ(z),energ(z+1)
      end if
    end if
!          write(50+j,*)energ(z),mat(i,z)
!          write(50+j,*)e(j),intens
!          write(50+j,*)energ(z+1),mat(i,z+1)
    newkin(j)=intens
!          write(29,*)t,e,intens
!        write(*,*) "energy(j) = " ,j, energ(j)
  end do
  king(:)=newking(:)
  kinu(:)=newkinu(:)
!         end do
return

end subroutine replacek

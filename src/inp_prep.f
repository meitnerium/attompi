	    program data_prep
	    
	    integer ndim,nx,nc,vmin,vmax,nmax,wf,ftype,nsin
	    real*8 r0,rmin,rmax,tmax,dt,atmass,rot1,rot2,e0
	    real*8 lambda,field,tau,sigma,rsmooth
	    character*50 c
	    namelist /datos/ ndim,c,nc,r0,rmin,rmax,nx,tmax,dt,lambda
     &                       ,field,tau,sigma,rsmooth,atmass
     &                       ,rot1,rot2,vmin,vmax,nmax,e0,wf
     &                    ,rot1,rot2,vmin,vmax,nmax,e0,wf
     &                    ,tp,tamin,tamax,pulseuvdt,tadt,na,ftype,nsin
    


    
	    open (8,file='0_data.input')
	    read(8,nml=datos)
            close (8)
	    
	    write (*,*) '0_data.input is readed'
	    
	    open(9,file='grid.inp')
	    write (9,91) ndim
	    write (9,92) r0
	    write (9,92) rmin
	    write (9,92) rmax
	    write (9,91) nx
	    close(9)

	    open(9,file='dynamics.inp')
	    write (9,92) tmax
	    write (9,92) dt
	    write (9,91) nc
	    write (9,91) wf
	    close(9)

	    open(9,file='field.inp')
	    write (9,92) lambda
	    write (9,92) field
	    write (9,92) tau
	    write (9,92) tp
	    write (9,91) ftype
	    write (9,91) nsin
	    close(9)

	    open(9,file='smooth.inp')
	    write (9,92) sigma
	    write (9,92) rsmooth
	    close(9)
	    
	    open(9,file='at_mass.inp')
	    write (9,92) atmass
	    close(9)

	    open(9,file='n_v.inp')
	    write (9,91) vmin
	    write (9,91) vmax
	    write (9,91) nmax
	    close(9)
	    
	    open(9,file='ptls.inp')
	    write (9,92) rot1
	    write (9,92) rot2
	    write (9,92) e0
	    close(9)

 91         format(I6)
 92         format(E22.15)
 
 ! EWdEe
 ! FW.d
 
            end



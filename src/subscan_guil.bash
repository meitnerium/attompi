#!/bin/bash
#PBS -N sin_400_N1_16_sept_2014
#PBS -A cjt-923-aa
#PBS -l walltime=02:15:00
#PBS -l nodes=4:ppn=16
#PBS -M meitnerium109@gmail.com
#PBS -m bea
module load MKL/10.3
module load ifort_icc/14.0
module load openmpi/1.6.3-intel
module load gnuplot/4.6.4
#intel_mpi/14.0 utiliser mpiifort
#cd /home/fradion12/git/atto/current/prog/results
cd "${PBS_O_WORKDIR}"
pwd
./all.bat
make scan
ifort -check all -fpe0  -traceback -debug extended -heap-arrays -g -o exp_op exp_op_jc_1d.f
cp scan ../calc
mv exp_op ../calc
echo ok
cd ../calc
./0_CALC
#./scan
mpirun -np 64 ./scan

        program kinetic_energy
	
	include 'limits.h'
        integer nx,i,itmp       ! grid point number, counter, integer temporal
        real*8 cm,pi            ! units transform coefficients
        real*8 kin              ! kinetic energy
        real*8 xmin,xmax,x0,dx  ! coordinate grid parameters
        real*8 fc,dp            ! impulse grid parameters
        real*8 x(ntotal)        ! grid at the coordinate space
        real*8 p(ntotal)        ! grid at the impulse space
        real*8 m                ! reduced mass
        real*8 tmp		! real temporal
c --------------------- input ------------------------------

        open (8,file='units.cft')
        read (8,*) cm
        close (8)

        open (8,file='at_mass.inp')
        read (8,*) m
        close (8)

        open (8,file='grid.unf',form='unformatted')
        read (8) itmp,itmp,nx
          do 93 j=1,nx
             read (8) x(j)
 93       continue
        close (8)
c --------------------- calc impulse grid ------------------------------
        PI=dacos(-1.D0)
        m=m*cm

        dx=x(2)-x(1)
        dp=1.d0/dx/nx
        fc=1/2.d0/dx
        do 60 k=1,nx/2+1             ! inverse for fourn (see Num.Rec.)
          p(k)=2.d0*pi*(k-1)*dp
 60     continue
        do 70 k=nx/2+2,nx
          p(k)=2.d0*pi*(-fc+(k-nx/2-1)*dp)
 70     continue
c --------------------- calc and output energy ------------------------------
        open (9,file='kin.unf')
        do 80 k=1,nx
         kin=p(k)*p(k)/m/2.d0
 80      write (9,*) kin
        close (9)
        write (*,*) 'kinetic energy - ok'
        end
c ===================================================

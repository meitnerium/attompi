c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      subroutine results_output(iv,in,ncall,it,nt,nst,p,wfast
     &       ,atto_norm_coef,dtfs,rrs,newkin,king,kinu,energ,iat)
      include 'limits.h'
      real*8 p(*)                    ! impulse grid
      !real*8 rrs(*)                   ! rR*sqrt(sin(th))
      real*8 rrs(4096),newkin(4096),energ(4096)
      real*8 king(4096),kinu(4096)                   ! rR*sqrt(sin(th))
      real*8 dtfs
      complex*16 wfast(ncm,*)       ! array for storage of analit. propagation results
      real*8 atto_norm_coef
      character*50 fn1,fn2,fn
      real*8 m,cev
      integer iat

      open (8,file='units.cft')
      read (8,*) cm
      read (8,*) cv,ct
      close (8)

      open (8,file='at_mass.inp')
      read (8,*) m
      close (8)
      m=m*cm	
      cev=27.212d0
      write(*,*) 'TEST!!!'
c      atto_norm_coef=1.d0
c      do i=1,4096
c         write(12346,*) i,abs(wfast(1,i))**2
c      end do

c      write(*,*) 'atto_norm_coef',atto_norm_coef
c      write(*,*) 'it , nt in output',it,nt 
      if (it.eq.nt) then
        write(fn,'(A6,I4.4)') "mkdir ",iat
        ret=SYSTEM(fn)
        write(fn1,'(I4.4,A1,I4.4,A7,I2.2,A1,I2.2,A4)') iat,"/",iat,
     &"_1_imp_d_",iv,"_",in,".dat"
        write(fn2,'(I4.4,A1,I4.4,A7,I2.2,A1,I2.2,A4)') iat,"/",iat,
     &"_2_imp_d_",iv,"_",in,".dat"
c        write(fn,'(I4.4,A7,I2.2,A1,I2.2,A4)') iat,"_imp_d_",iv,"_",
c     &in,".dat"

c           fn1='1_imp_d_'//char(iv+48)//'_'//char(in+48)//'.dat'
c           fn2='2_imp_d_'//char(iv+48)//'_'//char(in+48)//'.dat'
c           fn='imp_d_'//char(iv+48)//'_'//char(in+48)//'.dat'
       open (19, file=fn1)
c!        write(*,*) 'NSaT',nst

        rrs(1)=0.d0
           do i=1,nst
c              write(*,*) 'output:i,nst',i,nst
             rrs(i)=abs(wfast(1,i))**2*atto_norm_coef**2
             king(i)=rrs(i)
c	       write (19,*) p(i),rrs(i)
           write (19,*) p(i)*p(i)/2.d0/m*cev/2.d0,rrs(i)
        end do
           close(19)
	   open (19, file=fn2)
           do i=1,nst
c              write(*,*) "output: i,nst",i,nst
	      rrs(i)=abs(wfast(2,i))**2*atto_norm_coef**2
             kinu(i)=rrs(i)
c	      write (19,*) p(i),rrs(i)
	      write (19,*) p(i)*p(i)/2.d0/m*cev/2.d0,rrs(i)
	   end do
           close(19)
c	   open (19, file=fn)
           do i=1,nst
cc              write(*,*) 'i in output',i
	      rrs(i)=(abs(wfast(1,i))**2 + abs(wfast(2,i))**2)
     &                *atto_norm_coef**2
              newkin(i)=rrs(i)
c     		write (19,*) p(i),rrs(i)
c	      write (19,*) p(i)*p(i)/2.d0/m*cev/2.d0,rrs(i)
              energ(i)=p(i)*p(i)/2.d0/m*cev/2.d0
!              write(*,*) 'i newkin and energe in output',i,newkin(i),
!     &        energ(i)
	   end do
c           close(19)
c           fn='p.dat'
c	   open (19, file=fn)
c           do i=1,nst
cc	      write (19,*) p(i)
c	   end do
c           close(19)
	   

           fn1='1_imp_f_'//char(iv+48)//'_'//char(in+48)//'.dat'
           fn2='2_imp_f_'//char(iv+48)//'_'//char(in+48)//'.dat'
           fn='imp_f_'//char(iv+48)//'_'//char(in+48)//'.dat'
c	   open (19, file=fn1)
c           do i=1,nst
cc	   	write (19,*) p(i),wfast(1,i)
cc	      write (19,*) p(i)*p(i)/2.d0/m*cev/2.d0,wfast(1,i)
c	   end do
c           close(19)
c	   open (19, file=fn2)
c           do i=1,nst
cc	   	write (19,*) p(i),wfast(2,i)
cc	      write (19,*) p(i)*p(i)/2.d0/m*cev/2.d0,wfast(2,i)
c	   end do
c           close(19)
c	   open (19, file=fn)
c           do i=1,nst
c	   	write (19,*) p(i),wfast(1,i) + wfast(2,i)
c      write (19,*) p(i)*p(i)/2.d0/m*cev/2.d0,wfast(1,i) + wfast(2,i)
c	   end do
c           close(19)


	 else
	 
c           do i=1,nst
c	     rrs(i)=(abs(wfast(1,i))**2 + abs(wfast(2,i))**2)*atto_norm_coef**2
c	     rrs(i)=abs(wfast(1,i))**2*atto_norm_coef**2
c	     write (ncall+20,*) rrs(i)
	     write (999,*) 'calling rrs,ncall=',ncall
c	   end do
c	   write(15,*) (it-1)*dtfs
c           do i=1,nst
c	      rrs(i)=abs(wfast(2,i))**2*atto_norm_coef**2
c	      write (ncall+60,*) rrs(i)
c	   end do
	 end if
	 
	 
	 end

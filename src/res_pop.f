  	   program pop_res
	
	   integer vmax,nmax
	
	   character*20 fn
	   real*8 p(10000),d(10000),nvpop,dst(10000)

	   vmax=10
	   nmax=5
	
           open (8,file='grid.unf',form='unformatted')
           read (8) ndim,ntot,nr
           close (8)
	   nr=nr*4

           do i=1,nr
	     dst(i)=0.D0
	   end do
	
	   open (19,file='pop_vn.dat')
	   do jv=0,vmax
	     do jn=0,nmax
               read (19,*) jv1,jn1,nvpop
               fn='imp_d_'//char(iv+48)//'_'//char(in+48)//'.dat'
 	       open (9, file=fn)
                do i=1,nr
	         read (19,*) p(i),d(i)
		 dst(i)=dst(i)+nvpop*d(i)
	        end do
               close(9)
             end do
	   end do
           close(19)
	   
	   open (29,file='imp_dist_sum.dat')
             do i=1,nr
	       write (29,*) p(i),d(i)
	       dst(i)=dst(i)+nvpop*d(i)
	     end do
           close(29)
	   
	   write (*,*) 'common result - ok'
	   end

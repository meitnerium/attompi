program filtre




  integer ntmax
  parameter(ntmax=1000000)
  real*8  :: omega,tau,e0,e(4096*8),dE                  ! w
  integer :: na,newna
  integer :: i,nat,lnt,iat,j,itmp,jv
  integer :: ndim,rc                         ! problem dimension
  real*8  :: tamin,tamax,tp,tadt
  real*8  :: rmin,rmax
  integer :: nmax,ftype,nsin
  integer :: nc
  real*8  :: lambda,sigma,rsmooth
  real*8  :: r0,atmass,rot1,rot2
  real*8  :: pulseuvtd,meanta
  real*8  :: c(ntmax)                      ! field contour=E(t)
  real*8  :: dt,dtfs,tmax,dt2,cCmEv              ! time step value (au, fs) max time value ,dt*2 (for analit propagation)
  real*8  :: filterg(4096*8),filteru(4096*8),f1,f2,pi
  namelist /datos/ ndim,c,nc,r0,rmin,rmax,nx,tmax,dt,lambda,field,tau,sigma,rsmooth,atmass,rot1,rot2,vmin,vmax,nmax,e0,wf,tp,tamin,tamax,pulseuvdt,tadt,na,ftype,nsin
  open (8,file='0_data.input')
    read(8,nml=datos)
  close (8)
  pi=dacos(-1.d0)
  cCmEv=8065.5
  omega=(1.d0/lambda)/cCmEv
  write(*,*) "omega=",omega
  write(*,*) "pi=",pi
  write(*,*) "e0=",e0/27.212d0
  write(*,*) "e0-omega",e0-omega,"e0+omega",e0+omega
  dE=1.d-3
  newna=8
  do j=1,4096
    e(j)=(j-1)*dE+1.d-5
    f1=dsin(newna*(e(j)-e0)*pi/(2.d0*omega))
    f2=dsin((e(j)-e0)*pi/(2.d0*omega))
    filterg(j)=(f1/f2)
    f1=dsin(newna*(e(j)-e0-omega)*pi/(2.d0*omega))
    f2=dsin((e(j)-e0-omega)*pi/(2.d0*omega))
    filteru(j)=(f1/f2)
    write(*,*) e(j)/2.d0,filterg(j)*filterg(j),filteru(j)*filteru(j)
  end do
end program filtre

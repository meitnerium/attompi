        implicit integer (i-n)
	implicit real*8 (a-h,o-z)
        integer ntotal,ncm,nn,nxmax,ntmax
        parameter (ntotal=500000) ! max grid point number for all axis
        parameter (nxmax=500000)     ! max grid point number for one axis
        parameter (nn=2)          ! axis number (problem dimension)
        parameter (ncm=2)          ! axis number (problem dimension)
        parameter (ntmax=300000)   ! max grid point number for the time axis


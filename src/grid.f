      program space_grid
      
      include 'limits.h'
      
      integer nx(nn)                         ! ndim-counter and ndim points numbers
      real*8 x0(nn),xmin(nn),xmax(nn),dx(nn) ! grid parameters
      real*8 x(nn,nxmax)                     ! grid

c ------------------------- input block ----------------------------
        open (8,file='grid.inp')
        read (8,*) ndim  ! problem dimension
        do 10 in=1,ndim
            read (8,*) x0(in),xmin(in),xmax(in),nx(in) ! grid for axes in
 10     continue
        close (8)
c ------------------------- calculation block ----------------------------
        ntot=1
        do 15 j=1,ndim
            dx(j)=(xmax(j)-xmin(j))/(nx(j)-1)
            x(j,1)=xmin(j)
            do 20 k=2,nx(j)
              x(j,k)=x(j,k-1)+dx(j)
 20         continue
        ntot=ntot*nx(j)
 15     continue
	write(*,*) 'nx', nx,'ntot', ntot
c ------------------------- output ----------------------------
        open (9,file='grid.unf',form='unformatted')
        write (9) ndim,ntot,(nx(j),j=1,ndim)
        do 93 in=1,ndim
          do 93 j=1,nx(in)
 93         write (9) x(in,j)
        do 931 in=1,ndim
 931       write (9) x0(in)
        close (9)
        write (*,*) 'grid - ok'
        end


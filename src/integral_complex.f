c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	real*8 function filon_cos(tab,omeg,dt,nbpts,tmin)
c ---------------------------------------------------------
c calcul l'integrale de tmin a tmax de f(t)*cos(omeg*t)    -
c par la methode de filon.                                 -
c tab contient la fonction f en nbpts points espaces de dt.-
c nbpts doit etre pair
c ----------------------------------------------------------
	implicit double precision (a-h,o-x)
	implicit logical (y)
	dimension tab(nbpts)
c
	eps = 1.d-15
	theta = omeg*dt
	tsin = dsin(theta)
	t2sin = dsin(2.d0*theta)
	tcos = dcos(theta)
c
c
	n = nbpts
c	tmax = tmin + dt*(n-1)
	tmax = tmin + dt*(n-0.5D0)
	if (theta.lt.eps) then
	  a = 0.d0
	  b = 1.d0/3.d0
	  c = 4.d0/3.d0
	else
	  a = (theta**2 + theta*t2sin*0.5d0 - 2.d0*tsin**2)/theta**3
	  b = (theta + tcos**2*theta - t2sin)/theta**3
	  c = 4.d0*(tsin - theta*tcos)/theta**3
	endif
c
	sum = tab(1)*dcos(omeg*tmin) + tab(n)*dcos(omeg*tmax)  ! old variant
c
c
	sumc = 0.d0
	sumb = 0.d0
c
	do 10 i = 2,n - 1,2
c		t = tmin + (i-1)*dt
		t = tmin + (i-0.5D0)*dt
		sumc = sumc + tab(i)*dcos(omeg*t)
10      continue
	do 20 i = 3,n - 2,2
c		t = tmin + (i-1)*dt
		t = tmin + (i-0.5D0)*dt
		sumb = sumb + tab(i)*dcos(omeg*t)
20      continue
c
	sumb = sum + 2.d0*sumb
	temp = tab(1)*dsin(omeg*tmin) - tab(n)*dsin(omeg*tmax)
	sum = (-a*temp + b*sumb + c*sumc)
c
	filon_cos = sum*dt
	return
	end      
c _______________________________________________________________	
      Real*8 function simpson(f,k1,k2,dt) !
      INTEGER i1,i2
      real*8 sum,Sum1,Sum2,dt
      real*8 F(*)
      
      
      i1=k1
      i2=k2
      
      sum=0.D0      
      if (mod(i2-i1,2).ne.0) then
        sum=0.5D0*dt*(f(i1)+f(i1+1))
        i1=i1+1
      end if
      
      Sum1=0.D0
      DO K=i1+1,i2-1,2  !  *4
        Sum1=Sum1+F(k)
      ENDDO
      Sum2=0.D0
      DO K=i1+2,i2-2,2  ! *2
        Sum2=Sum2+F(k)
      ENDDO
      sum=sum+dt*(F(i1)+F(i2)+4.D0*Sum1+2.D0*Sum2)/3.D0
      simpson=sum
      return
      END
c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

       SUBROUTINE INTEGRAL (NDIM,X,NX,F,NTOT,INTG)
c Integral of a N-dimentional function of complex variable

       include 'limits.h'

       INTEGER NX(*),NTOT
       REAL*8 X(NN,NXMAX),dx1
       COMPLEX*16 INTG,INTG1
       complex*16 f(*),f1(nxmax)

       NTT=NTOT
       JDIM=1
c integrating for the first axes

  15   KK=1
       JJ1=1
       JJ2=NX(JDIM)

       DX1=X(JDIM,2)-X(JDIM,1)

  20   CONTINUE
       K=1
       DO 30 JJ=JJ1,JJ2
                F1(K)=F(JJ)
                K=K+1
  30   CONTINUE
       CALL INT_C1D (DX1,F1,INTG1,NX(JDIM))
       F(KK)=INTG1

       JJ1=JJ1+NX(JDIM)
       JJ2=JJ2+NX(JDIM)

       IF (KK.NE.NTT/NX(JDIM)) THEN
          KK=KK+1
          GOTO 20
       END IF

       NTT=NTT/NX(JDIM)
       JDIM=JDIM+1

       IF (JDIM-1.NE.NDIM) GOTO 15 ! integrating for the next axes

       INTG=INTG1

       END
C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      SUBROUTINE INT_C1D(DX,Y,INTEG,N)
      REAL*8 DX
      complex*16 INTEG,Y(*),SUM
C
      SUM=(0.d0,0.d0)
      DO 1 I=2,N-1
 1     SUM=SUM+Y(I)
      SUM=SUM+Y(1)/2.d0+Y(N)/2.d0
      INTEG=SUM*DX
      END
C +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
      SUBROUTINE INT_R1D(DX,Y,INTEG,N)
      REAL*8 DX
      REAL*8 INTEG,Y(*),SUM
C
      SUM=(0.d0,0.d0)
      DO 1 I=2,N-1
 1     SUM=SUM+Y(I)
      SUM=SUM+Y(1)/2.d0+Y(N)/2.d0
      INTEG=SUM*DX
      END

      SUBROUTINE INT_R1Dp(DX,Y,INTEG,N)
      REAL*8 DX
      REAL*8 INTEG,Y(*),SUM
C
      SUM=(0.d0,0.d0)
      DO 1 I=1,N
 1     SUM=SUM+Y(I)
      INTEG=SUM*DX
      END


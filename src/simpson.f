      Real*8 function integ(F,A,B,N) !
      INTEGER K,M
      REAL*8 A,B,H,Sum,SumEven,SumOdd,integ,X
      real*8 F(*)
      
      M=N/2
      H=(B-A)/(2*M)
      SumEven=0
      DO K=1,(M-1)
        X=A+H*2*K
        SumEven=SumEven+F(2*K)
      ENDDO
      SumOdd=0
      DO K=1,M
        X=A+H*(2*K-1)
        SumOdd=SumOdd+F(2*K-1)
      ENDDO
      Sum=H*(F(1)+F(2*M)+2*SumEven+4*SumOdd)/3
      integ=Sum
      RETURN
      END

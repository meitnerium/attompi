	subroutine prepare_wfv(wfv,cv,ptl,M,RMIN,RMAX,dx,erot,
     &                         ntot,nv)

        include 'limits.h'
	real*8 m,erot,rmin,rmax,dx,cv
	real*8 ptl(*)
        real*8 wfv(0:30,ntotal)            
        real*8 vec(ntotal)               
        real*8 ev(0:30),eviv
	
        open (8,file='spectr.input')
	  do iv=0,30
	    read (8,*,end = 10) itmp,ev(iv)
	    ev(iv)=ev(iv)*cv
	  end do
  10    continue
        nv=iv-1	  
        close (8)
c	write (*,*) 'nv=',nv
	
	do jv=0,nv
	  eviv=ev(jv)+erot
c          write (*,*) 'jv=',jv,' eviv=',eviv
          call getpsi(ptl,M,RMIN,RMAX,dx,eviv,ntot,VEC,ntotal)
          write (*,*) 'jv=',jv,' eviv=',eviv
	  do j=1,ntot
            wfv(jv,j)=vec(j)*(-1.D0)**jv
c	    write (10+jv,*) rmin+(j-1)*dx,wfv(jv,j)
          end do
        end do	  
        write (*,*) 'eigenfunctions - ok'
	
	end
c ========================================================================	
        subroutine test_v(wf,wfv,x,dx,nx,ntot,nv,nc)
        include 'limits.h'
        integer ntot,nx(*)
	real*8 dx
        real*8 wfv(0:30,ntotal)
        real*8 fv(ntotal)
        real*8 az(ntotal)
        real*8 wfc(0:30)
        real*8 wfnorm
        real*8 sum
        real*8 simpson
        real*8 x(nn,nxmax)
        real*8 calc_norm
        real*8 wfn
        complex*16 wf(ncm,ntotal)
        complex*16 wf1(ntotal)
        complex*16 z(ntotal)
        complex*16 wres
	
	sum=0
        wfn=calc_norm(ntot,1,wf,dx)**2
        call chan1(wf,wf1,ntot,1)    ! ground state
	do jv=0,nv
	  do j=1,ntot
	    fv(j)=wfv(jv,j)
	  end do
	  call mult_zara1c(ntot,wf1,fv,z)
          call integral (1,x,nx,z,ntot,wres)
	  wfc(jv)=abs(wres)**2
	  sum=sum+wfc(jv)
c	  write (1,*) jv,wfc(jv),sum
	end do
	write (82,*) wfn-sum
c	write (2,*) wfn
c	write (3,*) sum
c	stop
	end
c ========================================================================	
	

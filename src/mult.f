! za - complex array
! ra - rational array
! zc - complex const
! rc - rational const
! zres - resulting complex array
! rres - resulting rational array

c =================================================================
        subroutine mult_zarc (nc,n,za,rc,zres)
        include 'limits.h'
        complex*16 za(ncm,*),zres(ncm,*)
        real*8 rc
         do jc=1,nc
          do j=1,n
             zres(jc,j)=za(jc,j)*rc
          end do
         end do
        end
c =================================================================
        subroutine mult_zazc (nc,n,za,zc,zres)
        include 'limits.h'
        complex*16 za(ncm,*),zres(ncm,*),zc
         do jc=1,nc
          do j=1,n
             zres(jc,j)=za(jc,j)*zc
          end do
         end do
        end
c =================================================================
        subroutine mult_zaza (nc,n,za1,za2,zres)
        include 'limits.h'
        complex*16 za1(ncm,*),za2(ncm,*),zres(ncm,*)
         do jc=1,nc
          do j=1,n
             zres(jc,j)=za1(jc,j)*za2(jc,j)
          end do
         end do
        end
c =================================================================
        subroutine mult_rara (nc,n,ra1,ra2,res)
        include 'limits.h'
        real*8 res(nc,n)
	real*8 ra1(nc,n),ra2(nc,n)
         do jc=1,nc
          do j=1,n
             res(jc,j)=ra1(jc,j)*ra2(jc,j)
          end do
         end do
        end
c =================================================================
        subroutine mult_zara (nc,n,za,ra,zres)
        include 'limits.h'
        complex*16 za(ncm,*),zres(ncm,*)
	real*8 ra(ncm,*)
         do jc=1,nc
          do j=1,n
             zres(jc,j)=za(jc,j)*ra(jc,j)
          end do
         end do
        end
c =================================================================
        subroutine mult_zara1c (n,za,ra,zres)
        include 'limits.h'
        complex*16 za(*),zres(*)
	real*8 ra(*)
          do j=1,n
             zres(j)=za(j)*ra(j)
          end do
        end
c =================================================================
        subroutine absz (n,z,az)
        include 'limits.h'
        complex*16 z(*)
	real*8 az(*)
        do j=1,n
          az(j)=abs(z(j))
        end do
        end
c =================================================================
        subroutine absz2 (n,z,az)
        include 'limits.h'
        complex*16 z(*)
	real*8 az(*)
        do j=1,n
          az(j)=abs(z(j))**2
        end do
        end
c =================================================================
        subroutine sumabs2c(n,z,az)
        include 'limits.h'
        complex*16 z(ncm,*)
	real*8 az(*)
        do j=1,n
          az(j)=abs(z(1,j))**2 + abs(z(2,j))**2
        end do
        end
c =================================================================

      subroutine mult_eptl (x,y,n,nc)
      include 'limits.h'
      integer n,nc
      complex*16 x(ncm,ntotal),z(ncm),y(ncm,ncm,ntotal),sum
      do 10 i=1,n    ! for each point (real & imag)
        do 2 ic=1,nc  ! for each channel
	  sum=0.D0
          do 1 jc=1,nc  ! for each channel
	    sum=sum+y(ic,jc,i)*x(jc,i)
1         continue
          z(ic)=sum
2       continue
        do 3 ic=1,nc  ! for each channel
          x(ic,i)=z(ic)
3	continue  
10    continue
      end
c =================================================================
      subroutine mult_ekin (x,y,n,nc)
      include 'limits.h'

      integer n
      complex*16 x(ncm,ntotal),y(ntotal)
      do 2 i=1,n    ! for each point (real & imag)
        do 2 ic=1,nc  ! for each channel
             x(ic,i)=x(ic,i)*y(i)
  2   continue
      end
c =================================================================
      subroutine smooth (x,y,n,nc)
      include 'limits.h'

      integer n
      complex*16 x(ncm,ntotal)
      real*8 y(ntotal)
      do 2 i=1,n    ! for each point (real & imag)
        do 2 ic=1,nc  ! for each channel
             x(ic,i)=x(ic,i)*y(i)
  2   continue
      end
c =================================================================
      subroutine conj_arr(x,y,n,nc)
      include 'limits.h'

      integer n
      complex*16 x(ncm,ntotal),y(ncm,ntotal)
      do 2 i=1,n    ! for each point (real & imag)
        do 2 ic=1,nc  ! for each channel
             y(ic,i)=conjg(x(ic,i))
  2   continue
      end
c =================================================================
        subroutine mult (x,y,z,n)
        complex*16 x(n),y(n),z(n)
           do 10 k=1,n
             z(k)=x(k)*y(k)
  10       continue
        end
c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        subroutine Rrsin (x,y,n,sign,nc)
        include 'limits.h'
        integer n,sign
        complex*16 y(ncm,ntotal)
        real*8 x(*)

        if (sign.eq.1) then
        do 10 ic=1,nc  ! for each channel
          do 10 k=1,n
            y(ic,k)=y(ic,k)*x(k)
  10      continue
        end if

        if (sign.eq.-1) then
        do 20 ic=1,nc  ! for each channel
          do 20 k=1,n
            y(ic,k)=y(ic,k)/x(k)
  20      continue
        end if
        end
c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        real*8 function arg (z)
        complex*16 z,j
        parameter (j=(0.D0,1.D0))
        arg=dreal(-j*log(z/abs(z)))
        end
c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        subroutine chan1(f,f1,n,ic)
        include 'limits.h'
        integer ic,n
        complex*16 f(ncm,*)
        complex*16 f1(*)
        
        do 20 k=1,n
          f1(k)=f(ic,k)
 20     continue
        end
c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        subroutine chan1r(f,f1,n,ic)
        include 'limits.h'
        integer ic,n
        real*8 f(ncm,*)
        real*8 f1(*)
        
        do 20 k=1,n
          f1(k)=f(ic,k)
 20     continue
        end
c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        subroutine eq_arr(x,y,ntot)
        complex*16 x(*),y(*)

        do 10 i=1,ntot
  10      y(i)=x(i)
        end
c +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

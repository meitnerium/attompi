%-- 2013-05-24 02:00 --%
config;
%tamin=-100
%tamax=180
N1REP='sin_400_N1_24_juil_2014';
N8REP='sin_400_N8_24_juil_2014';
%config;
n=1;
figure(n);
n=n+1;
ffield=strcat(N1REP,'/calc/field.dat');

lfield=importdata(ffield);
plot(lfield(:,1),lfield(:,2));
print -f1 -painters -dpng -loose matfield.png
%figure(n);
%n=n+1;
fprintf('Begining of the analysis\n');
idata=true;   % does data are imported?
Temp=tamin:tadt:tamax;
ETEMP=Emin:DE:Emax;
[T,E]=meshgrid(Temp,ETEMP);
if (idata)
  
  %T=importdata('fort.9');
  %T=importdata('tap.dat');
  %fprintf('Import of T done\n');
  %E=importdata('fort.10');
  %E=importdata('energ2d.dat');
  %fprintf('Import of E done\n');
  
  N1fileg=strcat(N1REP,'/calc/newkin2dg.dat');
  
  %N1data=importdata('matout.dat');
%  newdata=importdata('newkin2d.dat');
  
  N1datag=importdata(N1fileg);
  N1datag=N1datag';
  

  N8fileg=strcat(N8REP,'/calc/newkin2dg.dat');
  
  
  N8datag=importdata(N8fileg);
  N8datag=N8datag';
  
  %newdata3=newdata2';
  fprintf('Import of newdata done\n');
  save 'resultats.mat';
  fprintf('the results have been saved to resultats.mat\n');
end
hold off;
%mesh(T,E,newdata);
%view(0,90);
%axis([0 40 0 1]);
%colorbar;
%xlabel('\tau (fs)');
%ylabel('E (eV)');
%set(gca,'FontSize',16);
%set(gca,'XTick',(-30:10:60));
%h = get(gca, 'xlabel');
%set(h,'FontSize',16);
%h = get(gca, 'ylabel');
%set(h,'FontSize',16);
%set(gcf, 'PaperPositionMode', 'auto');
%set(gcf, 'PaperUnits', 'inches', 'PaperSize', [5 4]);
%%print -f2 -painters -dpng -loose kin2d.png
%print -painters -depsc2 -loose finalPlot.eps
%print -painters -dpng -loose finalPlot
%print -painters -dpdf -loose finalPlot.pdf
%print -zbuffer -dpdf -loose finalPlot.pdf
%saveas( gcf(), 'finalPlot.png', 'png' );

lambda=0.000079;
cCmEv=8065.5;
cCmUa=8065.5/27.212;
n=8;
e0=0.2644;
photon=1.5694;
omega=(1/lambda)/cCmEv;

test=sin(n*(ETEMP-e0));
f1g=sin(n*(ETEMP-e0)*pi/(2*omega));
f2g=sin((ETEMP-e0)*pi/(2*omega));
filtreG=f1g./f2g;
figure('Name','this is the g channel');
n=n+1;
plot(ETEMP./2,filtreG.*filtreG);


f1u=sin(n*((ETEMP-e0)*pi/(2*omega)-pi/2));
f2u=sin((ETEMP-e0)*pi/(2*omega)-pi/2);
filtreU=f1u./f2u;
figure('Name','this is the u channel');
n=n+1;
plot(ETEMP./2,filtreU.*filtreU);

%	ee=0.001*e+0.000001;

%        f1g[e]=sin(n*(ee-e0)*pi/(2*omega));
%        f2g[e]=sin((ee-e0)*pi/(2*omega));
%        filtreG[e]=f1g[e]/f2g[e];
%        fprintf(fp1,"%f\t%f\n",ee/2,filtreG[e]*filtreG[e]/200);

%        f1u[e]=sin(n*((ee-e0)*pi/(2*omega)-pi/2));
%        f2u[e]=sin((ee-e0)*pi/(2*omega)-pi/2);
%        filtreU[e]=f1u[e]/f2u[e];
%    	fprintf(fp2,"%f\t%f\n",ee/2,filtreU[e]*filtreU[e]/20000);

[a,b]=size(N1datag)
for i=1,a
%for j=1,b
    i
    ETEMP(i)
    test2(i)=ETEMP(i)/2
%end 
end















figure('unit','pixel','position',[100 100 1024 512],'menubar','none','Name','this is the g channel');


%figure(n);
%n=n+1;
h=subplot(1,2,1);
p = get(h, 'pos');
p(1)=p(1)-0.06;
p(3) = p(3) + 0.1;
set(h, 'pos', p);
mesh(T,E,N1datag);
view(0,90);
axis([-200 tamax 0 1.2]);
colorbar('westoutside');
%set(gca, 'CLim', [0, 0.15]);
xlabel('\tau (fs)');
ylabel('E (eV)');
set(gca,'FontSize',16);
set(gca,'XTick',(-300:100:600));
h = get(gca, 'xlabel');
set(h,'FontSize',16);
h = get(gca, 'ylabel');
set(h,'FontSize',16);
set(gcf, 'PaperPositionMode', 'auto');
set(gcf, 'PaperUnits', 'inches', 'PaperSize', [10 4]);


%figure(n);
%n=n+1;
h=subplot(1,2,2);
p = get(h, 'pos');
p(1)=p(1)-0.06;
p(3) = p(3) + 0.1;
set(h, 'pos', p);

mesh(T,E,N8datag);
set(gca,'Position',get(gca,'position')+[0 0 0 0]);
view(0,90);
axis([-200 tamax 0 1.2]);
colorbar('eastoutside');
%set(gca, 'CLim', [0, 0.15]);
xlabel('\tau (fs)');
%ylabel('E (eV)');
set(gca,'FontSize',16);
set(gca,'XTick',(-100:100:600));
set(gca,'ytick',[]);

h = get(gca, 'xlabel');
set(h,'FontSize',16);
h = get(gca, 'ylabel');
set(h,'FontSize',16);
set(gcf, 'PaperPositionMode', 'auto');
set(gcf, 'PaperUnits', 'inches', 'PaperSize', [10 20]);

%title(gca,'you can also place a title like this');









